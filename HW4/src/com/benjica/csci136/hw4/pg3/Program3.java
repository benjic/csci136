package com.benjica.csci136.hw4.pg3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.benjica.csci136.hw4.pg3.ui.GeneratePanel;
import com.benjica.csci136.hw4.pg3.ui.LoginPanel;
import com.benjica.csci136.hw4.pg3.ui.UserPanel;
import com.benjica.csci136.hw4.pg3.ui.ViewPanel;
import com.benjica.csci136.hw4.pg3.users.GenerateUser;
import com.benjica.csci136.hw4.pg3.users.User;
import com.benjica.csci136.hw4.pg3.users.ViewUser;

/**
 * Driver Class for Program3
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program3 extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private User mUser;
	
	public Program3() {
		setTitle("Business Application");
		
		add(new LoginPanel(this));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		LoginPanel lp = ((LoginPanel) ((JButton) e.getSource()).getParent());
		JPanel userpanel;
		mUser = lp.getUser();
		
		getContentPane().removeAll();
		
		if ( mUser instanceof GenerateUser ) {
			userpanel = new GeneratePanel(mUser);
		} else if ( mUser instanceof ViewUser ) {
			userpanel = new ViewPanel(mUser);			
		} else {
			userpanel = new UserPanel(mUser);
		}
		
		setContentPane(userpanel);
		pack();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Program3 pg3 = new Program3();
		
		pg3.pack();
		pg3.setVisible(true);
	}

	
	
}
