package com.benjica.csci136.hw4.pg3.data;

import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * A table adapter for the FileArrayList that realizes the report type.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class ReportFileArrayTable extends FileArrayList<Report> implements
		TableModel, Serializable {

	private static final long serialVersionUID = 1L;
	transient private ArrayList<TableModelListener> listeners = new ArrayList<TableModelListener>();

	public ReportFileArrayTable() {
		super("data/pg3/reports.ser");
	}
	
	@Override
	public boolean add(Report e) {
		for (TableModelListener l : listeners) {
			l.tableChanged(new TableModelEvent(this));
		}
		return super.add(e);
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		listeners.add(arg0);
	}

	@Override
	public Class<?> getColumnClass(int arg0) {
		return String.class;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public String getColumnName(int arg0) {
		return arg0 == 0 ? "Generator" : "Date" ;
	}

	@Override
	public int getRowCount() {
		return size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		return arg1 == 0 ? get(arg0).getUser() : get(arg0).getDate();
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		listeners.remove(arg0);
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// Noop
	}
}
