package com.benjica.csci136.hw4.pg3.data;

import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

import com.benjica.csci136.hw4.pg3.users.User;

/**
 * A data structure for reports.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Report implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String mUser;
	private Date mDate = new Date();
	private File mReportFile;
	
	public Report(User u) {
		mUser = u.getUsername();
		
		mReportFile = new File("data/pg3/reports/" + mDate.getTime() + ".txt");
		
		try {
			FileWriter fw = new FileWriter(mReportFile);
			
			DateFormat fmt = DateFormat.getInstance();
			fw.write(String.format("%s created this report on %s", mUser, fmt.format(mDate) ));
			fw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public String getUser() {
		return mUser;
	}
	
	public String getDate() {
		DateFormat fmt = DateFormat.getInstance();
		
		return fmt.format(mDate);
	}
	
	public void openReport() {
		try {
			Desktop.getDesktop().open(mReportFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
