package com.benjica.csci136.hw4.pg3.data;

import java.text.DateFormat;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * A table adapter for the FileArrayList that realizes the request type.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class RequestFileArrayTable extends FileArrayList<Request> implements
		TableModel {

	private static final long serialVersionUID = 1L;

	public RequestFileArrayTable() {
		super("data/pg3/requests.ser");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub

	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch( columnIndex ) {
		case 0:
			return "Requestor";
		default:
			return "Date";
		}
	}

	@Override
	public int getRowCount() {
		return size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if ( columnIndex == 0 ) {
			return get(rowIndex).getUser();
		} else {
			DateFormat fmt = DateFormat.getInstance();
			return fmt.format(get(rowIndex).getRequestDate());
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		

	}
}
