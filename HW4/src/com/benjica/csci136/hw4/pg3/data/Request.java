package com.benjica.csci136.hw4.pg3.data;

import java.io.Serializable;
import java.util.Date;

/**
 * A datatype for the request object
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Request implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String mUser;
	private Date mRequestDate = new Date();
	
	public String getUser() {
		return mUser;
	}
	
	public Date getRequestDate() {
		return mRequestDate;
	}

	public void setUser(String username) {
		mUser = username;
	}
}
