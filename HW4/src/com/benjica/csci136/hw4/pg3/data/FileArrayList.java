package com.benjica.csci136.hw4.pg3.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * An abstraction ontop of an ArrayList that persists lists
 * to disk.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 * @param <E>
 */
public class FileArrayList<E> extends ArrayList<E> implements Serializable {
		
	private static final long serialVersionUID = 1L;
	private String mPath;
	public FileArrayList(String path) {
		super();
		
		mPath = path;
		
		try {
			
			FileInputStream mFileIn = new FileInputStream(mPath);
			
			ObjectInputStream in = new ObjectInputStream(mFileIn);
			
			addAll( ((ArrayList<E>) in.readObject()));
			
			in.close();
			mFileIn.close();
		} catch (IOException e) {
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			//e.printStackTrace();
		} 
	}

	@Override
	public boolean add(E e) {
		
		return ( super.add(e) && saveToDisk() );
	}

	@Override
	public void add(int index, E element) {
		
		super.add(index, element);
		saveToDisk();
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		return ( super.addAll(c) && saveToDisk());
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return ( super.addAll(index, c) && saveToDisk() );
	}

	@Override
	public E remove(int index) {
		
		E e = super.remove(index);
		saveToDisk();
		
		return e;
	}

	@Override
	public boolean remove(Object o) {
		return ( super.remove(o) && saveToDisk() );
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return ( super.removeAll(c) && saveToDisk() );
	}

	@Override
	protected void removeRange(int fromIndex, int toIndex) {
		super.removeRange(fromIndex, toIndex);
		saveToDisk();
	}
	
	private boolean saveToDisk() {
		
		try {
			FileOutputStream mFileOut = new FileOutputStream(mPath);
			ObjectOutputStream out = new ObjectOutputStream(mFileOut);
			out.writeObject(this);
			out.close();
			mFileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return true;
		
	}
	
	
}
