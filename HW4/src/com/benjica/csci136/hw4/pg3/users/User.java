package com.benjica.csci136.hw4.pg3.users;

/**
* A class to abstract users that can request reports
* Apr 13, 2014
*
* @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
*/
public class User {
	protected String mUserName;
	
	public User(String username) {
		mUserName = username;
	}

	public String getUsername() {
		// TODO Auto-generated method stub
		return mUserName;
	}
	
}
