package com.benjica.csci136.hw4.pg3.users;

import java.io.Serializable;

/**
 * A class to designate generating users
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class GenerateUser extends ViewUser implements Serializable {

	public GenerateUser(String username) {
		super(username);
		// TODO Auto-generated constructor stub
	}
	
}
