package com.benjica.csci136.hw4.pg3.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

import com.benjica.csci136.hw4.pg3.data.Request;
import com.benjica.csci136.hw4.pg3.users.User;

/**
 * A panel to allow users make report requests
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class UserPanel extends RequestPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	
	public UserPanel(User user) {
		super(user);
		
		JLabel mUserLabel = new JLabel("You are logged in as a user. You can request a report be generated below.");
		JButton mUserButton = new JButton("Request Report");
		
		mUserButton.addActionListener(this);
		
		add(mUserLabel);
		add(mUserButton);
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		Request r = new Request();
		r.setUser(mUser.getUsername());
		
		mRequests.add(r);
	}
}