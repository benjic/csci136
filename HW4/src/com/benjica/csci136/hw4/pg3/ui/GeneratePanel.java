package com.benjica.csci136.hw4.pg3.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.benjica.csci136.hw4.pg3.data.Report;
import com.benjica.csci136.hw4.pg3.users.User;

/**
 * This panel offers the user the abiltity to see requests and 
 * create new reports.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class GeneratePanel extends RequestPanel implements ActionListener {

	private GroupLayout mLayout;
	
	public GeneratePanel(User user) {
		super(user);
		setPreferredSize(new Dimension(400,300));
		JLabel tableLabel = new JLabel("Requests");
		JLabel optionsLabel = new JLabel("Tools");
		
		JButton genReportButton = new JButton("Generate Report");
		genReportButton.addActionListener(this);
		
		JTable table = new JTable(mRequests);
		JScrollPane scroll = new JScrollPane(table);
		
		JTable rTable = new JTable(mReports);
		rTable.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if ( e.getClickCount() == 2 ) {
					JTable t = ((JTable) e.getSource());
					
					mReports.get(t.getSelectedRow()).openReport();
				}
			}
			
		});
		JScrollPane reportScroll = new JScrollPane(rTable);
		
		mLayout = new GroupLayout(this);
		this.setLayout(mLayout);
		
		mLayout.setHorizontalGroup(
				mLayout.createSequentialGroup()
					.addGroup(
							mLayout.createParallelGroup(Alignment.CENTER)
								.addComponent(tableLabel)
								.addComponent(scroll))
					.addGroup(
							mLayout.createParallelGroup(Alignment.CENTER)
								.addComponent(optionsLabel)
								.addComponent(reportScroll)
								.addComponent(genReportButton)));
		
		mLayout.setVerticalGroup(
				mLayout.createSequentialGroup()
					.addGroup(
							mLayout.createParallelGroup()
								.addGroup(
										mLayout.createSequentialGroup()
											.addComponent(tableLabel)
											.addComponent(scroll))
								.addGroup(
										mLayout.createSequentialGroup()
											.addComponent(optionsLabel)
											.addComponent(reportScroll)
											.addComponent(genReportButton))
							));
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		mReports.add(new Report(mUser));
	}
	
}
