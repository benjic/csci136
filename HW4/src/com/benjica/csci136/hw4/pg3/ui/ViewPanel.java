package com.benjica.csci136.hw4.pg3.ui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.benjica.csci136.hw4.pg3.users.User;

/**
 * A view that allows view users to open generated reports.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class ViewPanel extends RequestPanel {

	private static final long serialVersionUID = 1L;

	public ViewPanel(User user) {
		super(user);

		JLabel directionsLabel = new JLabel("You can double click any generated report to open it.");
		JTable rTable = new JTable(mReports);
		rTable.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if ( e.getClickCount() == 2 ) {
					JTable t = ((JTable) e.getSource());

					mReports.get(t.getSelectedRow()).openReport();
				}
			}

		});
		JScrollPane reportScroll = new JScrollPane(rTable);

		GroupLayout mLayout = new GroupLayout(this);
		setLayout(mLayout);

		mLayout.setHorizontalGroup(
				mLayout.createParallelGroup()
				.addComponent(directionsLabel)
				.addComponent(reportScroll));

		mLayout.setVerticalGroup(
				mLayout.createSequentialGroup()
				.addComponent(directionsLabel)
				.addComponent(reportScroll));
	}
}
