package com.benjica.csci136.hw4.pg3.ui;

import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.benjica.csci136.hw4.pg3.users.GenerateUser;
import com.benjica.csci136.hw4.pg3.users.User;
import com.benjica.csci136.hw4.pg3.users.ViewUser;

/**
 * Login panel abstracts user authentication by getting an identifier
 * and level.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class LoginPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private static final String[] COMBO_VALUES = {"Request Reports", "View Reports", "Generate Reports" };
	
	private JLabel mLoginNameLabel;
	private JLabel mLoginTypeLabel;
	
	private JTextField mLoginNameTextField;
	private JComboBox<String> mLoginTypeComboBox;
	
	private JButton mSubmitButton;
	private GroupLayout mLayout;
	
	private ActionListener mAction;
	
	public LoginPanel(ActionListener loginAction) {
		
		mAction = loginAction;
		
		mLoginNameLabel = new JLabel("Login Name");
		mLoginTypeLabel = new JLabel("Login Type");
		
		mLoginNameTextField = new JTextField();
		mLoginTypeComboBox = new JComboBox<String>(COMBO_VALUES);
		
		mSubmitButton = new JButton("Login");
		mSubmitButton.addActionListener(mAction);
		
		mLayout = new GroupLayout(this);
		setLayout(mLayout);
		
		mLayout.setHorizontalGroup(
				mLayout.createParallelGroup(Alignment.TRAILING)
					.addGroup(
							mLayout.createSequentialGroup()
								.addComponent(mLoginNameLabel)
								.addComponent(mLoginNameTextField))
					.addGroup(
							mLayout.createSequentialGroup()
								.addComponent(mLoginTypeLabel)
								.addComponent(mLoginTypeComboBox))
					.addComponent(mSubmitButton)
		);
		
		mLayout.setVerticalGroup(
				mLayout.createSequentialGroup()
					.addGroup(
							mLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(mLoginNameLabel)
								.addComponent(mLoginNameTextField))
					.addGroup(
							mLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(mLoginTypeLabel)
								.addComponent(mLoginTypeComboBox))
					.addComponent(mSubmitButton)
		);
		
	}
	
	public User getUser() {
		switch ( mLoginTypeComboBox.getSelectedIndex()) {
		case 2:
			return new GenerateUser(mLoginNameTextField.getText());
		case 1:
			return new ViewUser(mLoginNameTextField.getText());
		default:
			return new User(mLoginNameTextField.getText());	
		}
	}
}
