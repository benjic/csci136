package com.benjica.csci136.hw4.pg3.ui;

import javax.swing.JPanel;

import com.benjica.csci136.hw4.pg3.data.ReportFileArrayTable;
import com.benjica.csci136.hw4.pg3.data.RequestFileArrayTable;
import com.benjica.csci136.hw4.pg3.users.User;

/**
 * An abstract class representing all the views.
 * Apr 13, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public abstract class RequestPanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	protected RequestFileArrayTable mRequests = new RequestFileArrayTable();
	protected ReportFileArrayTable mReports = new ReportFileArrayTable();
	protected User mUser;
	
	public RequestPanel(User user) {
		mUser = user;
	}
	
}
