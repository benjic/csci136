package com.benjica.csci136.hw4.pg2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.Administrator;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.EmergencyEmployee;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.Principal;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.SchoolEmployee;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.Superintendent;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.Teacher;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks.PaySalaries;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks.SchoolEmergency;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks.SchoolFire;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks.SchoolTask;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks.TeachStudents;

/**
 * Driver for Program2
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program2 {

	public static void main(String[] args) throws FileNotFoundException {
		// Utility Classes
		Scanner fileScanner = new Scanner(new File("data/employees.txt"));
		
		// Containers
		List<SchoolEmployee> employees = new ArrayList<SchoolEmployee>();
		List<SchoolTask> tasks = new ArrayList<SchoolTask>();
		
		// Build employee list
		while ( fileScanner.hasNextLine() ) {
			String[] params = fileScanner.nextLine().split(",");
			
			switch ( Integer.parseInt(params[0]) ) {
			case 1:
				employees.add( new Administrator(params[1], params[2], params[3]));
				break;
			case 2:
				employees.add( new Principal(params[1], params[2], params[3]));
				break;
			case 3:
				employees.add( new Superintendent(params[1], params[2], params[3]));
				break;
			case 4:
				employees.add( new Teacher(params[1], params[2], params[3]));
				break;
			}
		}
		
		fileScanner.close();
		
		fileScanner = new Scanner(new File("data/tasks.txt"));
		
		// Build task list
		while ( fileScanner.hasNextLine() ) {
			String[] params = fileScanner.nextLine().split(",");
			
			switch( Integer.parseInt(params[0]) ) {
			case 1:
				tasks.add( new PaySalaries());
				break;
			case 2:
				tasks.add( new SchoolFire());
				
			case 3:
				tasks.add( new TeachStudents());
			}
		}
		
		fileScanner.close();
		
		System.out.printf("Welcome to the school taks designator program!\n");
		
		// Iterate over the tasks
		for ( SchoolTask task : tasks ) {
			System.out.printf("New task: %s\n", task.getTaskDescription());
			
			// Mixup the employee list so that different employees
			// are able to handle events instead of the top n
			Collections.shuffle(employees);
			
			// Iterate over employee list
			for ( SchoolEmployee employee : employees ) {
			
				// Speaks for itself
				if ( task.canDoTask(employee)) {
					
					// Speaks for itself
					if ( task instanceof SchoolEmergency) {
						// We are guranteed by the CanDoTask that this person
						// inheirits from EmergencyEmployee and is castable
						// to handle this specific event
						System.out.printf("\t My name is %s and %s and %s\n",
								employee.getName(),
								employee.WhatIDo(),
								((EmergencyEmployee) employee).HowIHandleEmergencies());
					} else {
						System.out.printf("\t My name is %s and %s and %s\n",
								employee.getName(),
								employee.WhatIDo(),
								employee.GoToWork());
					}
					// A single employee is need for a single task
					// break so more employees don't to do the same task
					break;
				}
			}
		}

	}
	
}
