package com.benjica.csci136.hw4.pg2.SchoolProgram.Employees;


/**
 * TODO Implement Class Definition for Teacher.java
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Teacher extends SchoolEmployee {

	public Teacher(String name, String address, String phone) {
		super(name, address, phone);
	}

	@Override
	public String GoToWork() {
		return String.format("Hello, my name is %s and I am an teacher and I am going to work.", this.mName);
	}

	@Override
	public String WhatIDo() {
		return String.format("I teach things");
	}
}
