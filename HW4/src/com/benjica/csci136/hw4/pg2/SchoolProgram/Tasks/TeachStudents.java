package com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks;

import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.Teacher;

public class TeachStudents extends SchoolTask {
	
	public TeachStudents() {
		this.mEmployeeType = Teacher.class;
		this.mTaskDescription = "These kids need to be taught";
	}
}
