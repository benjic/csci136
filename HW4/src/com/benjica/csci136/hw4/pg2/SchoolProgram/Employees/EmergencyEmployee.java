package com.benjica.csci136.hw4.pg2.SchoolProgram.Employees;

/**
 * This interface defines the method needed for emergency trained
 * employees.
 * @author benjica
 *
 */
public interface EmergencyEmployee {
	/**
	 * This describes how the employee will handle the emergency
	 * @return a description of how they handle the emergency
	 */
	String HowIHandleEmergencies();

}
