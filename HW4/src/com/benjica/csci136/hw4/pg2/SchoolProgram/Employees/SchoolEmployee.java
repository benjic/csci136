package com.benjica.csci136.hw4.pg2.SchoolProgram.Employees;

/**
 * A parent class for all types of school employees
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public abstract class SchoolEmployee {
	
	protected String mName;
	protected String mAddress;
	protected String mPhone;
	
	public SchoolEmployee() {
		mName 		= "Generic Employee";
		mAddress 	= "123 Main Street";
		mPhone		= "123-456-9876";
	}
	
	public SchoolEmployee(String name, String address, String phone) {
		mName 		= name;
		mAddress	= address;
		mPhone		= phone;
	}
	
	public abstract String GoToWork();
	public abstract String WhatIDo();
	
	public String getName() {
		return mName;
	}
	public void setName(String name) {
		mName = name;
	}
	public String getAddress() {
		return mAddress;
	}
	public void setAddress(String address) {
		mAddress = address;
	}
	public String getPhone() {
		return mPhone;
	}
	public void setPhone(String phone) {
		mPhone = phone;
	}
}
