package com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks;

import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.SchoolEmployee;

/**
 * The parent class for tasks.
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public abstract class SchoolTask {
	
	// Attributes
	protected String mTaskDescription;
	protected Class<? extends SchoolEmployee> mEmployeeType;
	
	public SchoolTask() {
		mTaskDescription = "Someone needs to do this generic task.";
		mEmployeeType = SchoolEmployee.class;
	}
	
	public boolean canDoTask(SchoolEmployee e) {
		// This is a interesting hack that allows any child of SchoolEmployee
		// to do any SchoolTask that does not override this method.
		return e.getClass().equals(mEmployeeType);
	}
	
	public String getTaskDescription() {
		return mTaskDescription;
	}
}
