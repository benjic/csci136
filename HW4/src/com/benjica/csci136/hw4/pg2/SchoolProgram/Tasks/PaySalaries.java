package com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks;

import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.Administrator;

/**
 * TODO Implement Class Definition for PaySalaries.java
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class PaySalaries extends SchoolTask {
	
	public PaySalaries() {
		this.mEmployeeType = Administrator.class;
		this.mTaskDescription = "Someone needs to pay some salaries.";
	}
}
