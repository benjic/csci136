package com.benjica.csci136.hw4.pg2.SchoolProgram.Employees;

/**
 * TODO Implement Class Definition for Superintendent.java
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Superintendent extends SchoolEmployee implements EmergencyEmployee {

	public Superintendent(String name, String address, String phone) {
		super(name, address, phone);
	}

	@Override
	public String GoToWork() {
		return String.format("Hello, my name is %s and I am an superintendent and I am going to work.", this.mName);
	}

	@Override
	public String WhatIDo() {
		return String.format("I superintend things.");
	}

	@Override
	public String HowIHandleEmergencies() {
		return String.format("I am an superintendent and I am handling this emergency with super powers.");
	}
}
