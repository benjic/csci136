package com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks;

import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.EmergencyEmployee;
import com.benjica.csci136.hw4.pg2.SchoolProgram.Employees.SchoolEmployee;

/**
 * Any task that is an emergency
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class SchoolEmergency extends SchoolTask {

	/**
	 * Emergency tasks override the canDoTask and check
	 * to see if the employee can handle emergencies. This design allows
	 * all types of employees respond to emeregencies without having to 
	 * inherit from a parent emergency employee.
	 * 
	 * This models real life as teachers, admins, etc are not hired from
	 * a pool of trained Firmen or EMTs. They are trained to implement 
	 * the emergency interface.
	 */
	@Override
	public boolean canDoTask(SchoolEmployee e) {
		
		if ( e instanceof EmergencyEmployee)
			return true;
		
		return super.canDoTask(e);
	}
	
}
