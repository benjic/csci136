package com.benjica.csci136.hw4.pg2.SchoolProgram.Tasks;


/**
 * TODO Implement Class Definition for SchoolFire.java
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class SchoolFire extends SchoolEmergency {
	
	public SchoolFire() {
		this.mTaskDescription = "There is a fire!";
	}
	
}
