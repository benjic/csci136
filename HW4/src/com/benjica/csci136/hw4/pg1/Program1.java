package com.benjica.csci136.hw4.pg1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import com.benjica.csci136.hw4.pg1.linkedin.FirstConnection;
import com.benjica.csci136.hw4.pg1.linkedin.Person;
import com.benjica.csci136.hw4.pg1.linkedin.SecondConnection;

/**
 * A driver for the linked in model.
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program1 {

	public static void main(String[] args) throws FileNotFoundException {
	
		// Datastructs for random people generation
		List<Person> network = new ArrayList<Person>();
		List<String> assoc = new ArrayList<String>();
		
		// Utility Classes
		Scanner fileScanner = new Scanner(new File("data/names.txt"));
		Random rand = new Random();
		
		// Open names and create list of people
		while ( fileScanner.hasNext() ) {
			network.add( new Person(fileScanner.nextLine().split(" ")));
		}
				
		fileScanner.close();

		// Open assocations  and create list
		fileScanner = new Scanner(new File("data/associations.txt"));
		
		while ( fileScanner.hasNextLine() ) {
			assoc.add( fileScanner.nextLine());
		}
				
		fileScanner.close();
		
		// Go over each person in the list
		for ( Person p : network ) {
			// Create a random number of second connections
			for (int i = 0; i < rand.nextInt(10) + 5; i++) {
				
				// Create a connection and use a random assocation
				FirstConnection fc = new FirstConnection(network.get(rand.nextInt(network.size())));
				fc.setRelationship(assoc.get(rand.nextInt(assoc.size())));
				p.addConnection(fc);
				
				
				// Create random second connection pool
				for ( int j = 0; j < rand.nextInt(10) + 10; j++) {
					// Grab a random person
					Person p3 =  network.get(rand.nextInt(network.size()));
					
					// Creaet a second connection given the previous person
					SecondConnection sc = new SecondConnection(p3);
					sc.setAssociation(fc.mFirstName + " " + fc.getLastName());
					p.addConnection(sc);
					
				}
				
			}
		}
		
		// Grab a lucky single person
		Person lucky = network.get(rand.nextInt(network.size()));
		
		System.out.printf("We picked a person named %s %s\n", lucky.mFirstName, lucky.mLastName);
		
		// And print out thier connections
		for ( Person p : lucky.getConnections() ) {
			System.out.println("\t" + p);
		}
		
	}
	
}
