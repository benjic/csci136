package com.benjica.csci136.hw4.pg1.linkedin;


/**
 * A second conneciton is a person who is linked by an assocaited person.
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class SecondConnection extends Person {
	public SecondConnection(Person p) {
			this.mFirstName = p.mFirstName;
			this.mLastName = p.mLastName;
	}

	private String mAssociation;

	public String getAssociation() {
		return mAssociation;
	}

	public void setAssociation(String association) {
		mAssociation = association;
	}
	
	public String toString() {
		return String.format("%s %s is a second connection by %s", mFirstName, mLastName, mAssociation);
	}
	
}
