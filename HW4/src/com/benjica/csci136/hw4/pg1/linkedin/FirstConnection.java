package com.benjica.csci136.hw4.pg1.linkedin;


/**
 * A first connection is identified by some relationship
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class FirstConnection extends Person {
	
	public FirstConnection(Person p) {
		super();
		this.mFirstName = p.mFirstName;
		this.mLastName = p.mLastName;
	}
	protected String mRelationship;

	public String getRelationship() {
		return mRelationship;
	}

	public void setRelationship(String relationship) {
		mRelationship = relationship;
	}
	
	public String toString() {
		return String.format("%s %s is a first connection by %s", mFirstName, mLastName, mRelationship);
	}
}