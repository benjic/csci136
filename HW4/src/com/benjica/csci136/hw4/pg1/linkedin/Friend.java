package com.benjica.csci136.hw4.pg1.linkedin;

/**
 * TODO Implement Class Definition for Friend.java
 * Apr 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Friend extends Person {
	
	public String getFirstName() {
		return mFirstName;
	}
	
	public String getLastName() {
		return mLastName;
	}
	
	public void test() {
		
	}
}
