package com.benjica.csci136.hw4.pg1.linkedin;


/**
 * TODO Implement Class Definition for ThirdConneciton.java
 * Apr 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class ThirdConnection extends SecondConnection {
	
	public ThirdConnection(Person p) {
		super(p);
		// TODO Auto-generated constructor stub
	}
	
	public String toString() {
		return String.format("%s %s is a Third connection.", mFirstName, mLastName);
	}
}
