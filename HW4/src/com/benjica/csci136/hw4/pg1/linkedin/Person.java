package com.benjica.csci136.hw4.pg1.linkedin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A Parent data structure for Linked in Stuff.
 * Apr 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Person {
	
	// Fields
	
	public String mFirstName;
	public String mLastName;
	public String mAddress;
	public String mPhoneNumber;
	public String mEmail;
	public String mBirthday;
	
	protected List<Person> mConnections;
	
	public Person() {
		
		// Initialize Fields
		mFirstName 		= "";
		mLastName 		= "";
		mAddress 		= "";
		mPhoneNumber 	= "";
		mEmail			= "";
		mBirthday		= "";
		
		mConnections = new ArrayList<Person>();
		
	}
	
	public Person(String[] name) {
		mFirstName = name[0];
		mLastName = name[1];
		
		mAddress 		= "";
		mPhoneNumber 	= "";
		mEmail			= "";
		mBirthday		= "";
		mConnections = new ArrayList<Person>();
	}
	
	public void addConnection(Person p) {
		
		mConnections.add(p);
	}
	
	public String getFirstName() {
		return mFirstName;
	}

	public String getLastName() {
		return mLastName;
	}

	public String getAddress() {
		return mAddress;
	}

	public String getPhoneNumber() {
		return mPhoneNumber;
	}

	public String getEmail() {
		return mEmail;
	}

	public String getBirthday() {
		return mBirthday;
	}

	public Collection<Person> getConnections() {
		return mConnections;
	}
	
}
