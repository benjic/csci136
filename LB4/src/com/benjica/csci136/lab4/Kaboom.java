package com.benjica.csci136.lab4;

import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;

/**
 * A simple driver for the kaboom application.
 * Mar 8, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Kaboom extends JFrame {
	
	/**
	 * Initialize a new Kaboom Game.
	 * @throws IOException
	 */
	public Kaboom() throws IOException {
		setPreferredSize(new Dimension(700,700));
		GamePanel gp = new GamePanel();
		
		add(gp);
		
	}
	
	/**
	 * Entry to program
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Kaboom k = new Kaboom();
		
		k.pack();
		k.setVisible(true);

	}
	
}
