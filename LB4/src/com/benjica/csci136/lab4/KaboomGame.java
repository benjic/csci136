package com.benjica.csci136.lab4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Timer;

/**
 * An abstraction for the state of the game.
 * Mar 8, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class KaboomGame {
	
	// Two arrays to store the state of the game
	private boolean[] mMask;
	private boolean[] mField;
	
	// A timing mechanism to countdown
	public Timer mTimer;
	private int mSeconds;
	
	private int mSize;
	
	public KaboomGame( int boardSize, int numberOfEnimies, int duration ) {
		
		// Initialize all the things
		mSize 	= boardSize;
		mMask 	= new boolean[mSize*mSize];
		mField 	= new boolean[mSize*mSize];
		mSeconds = duration;
		mTimer = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Deincrement every second event
				mSeconds--;
			}
			
		});
		
		// Populate the field with baddies
		Random rand = new Random();
		for ( int i = 0; i < numberOfEnimies; i++) {
			mField[rand.nextInt(mSize*mSize)] = true;
		}
	}
	
	/**
	 * Getter for mask
	 * @return
	 */
	public boolean[] getMask() {
		return mMask;
	}
	
	/**
	 * Getter for field
	 * @return
	 */
	public boolean[] getField() {
		return mField;
	}
	
	/**
	 * For printing, yes.
	 */
	public String toString() {
		return String.format("Attempts: %3d Misses: %3d Hits: %3d\n", numberOfAttempts(), numberOfMisses(), numberOfHits());
	}
	
	/**
	 * This function gives proximity counts for the index. This is used to 
	 * help users strategically play the game.
	 * @param index
	 * @return
	 */
	public int countNeighbores(int index) {
		int count = 0;
		
		// Center Rows
		if ( (index - 1) > 0 && mField[index-1] )
			count++;
		if ( (index + 1) < mField.length && mField[index+1] )
			count++;
		
		// Left Rows
		if ( (index - mSize - 1) > 0 && mField[index - mSize - 1] )
			count++;
		if ( (index - mSize) > 0 && mField[index - mSize])
			count++;
		if ( (index - mSize + 1) > 0 && mField[index - mSize + 1] )
			count++;
		
		// Right Rows
		if ( (index + mSize - 1) < mField.length && mField[index+mSize-1] )
			count++;
		if ( (index + mSize) < mField.length && mField[index+mSize])
			count++;
		if ( (index + mSize + 1) < mField.length && mField[index+mSize+1] )
			count++;
		
		
		return count;
	}
	
	/**
	 * A boolean test to see if all baddies are revealed
	 * @return
	 */
	public boolean hasWon() {
		
		// Iterate and match each baddie with a true mask
		for (int i = 0; i < mSize*mSize; i++) {
			if ( mField[i] && !mMask[i])
				return false;
		}
		
		return true;
	}
	
	/**
	 * Get the total number of bombs used
	 * @return
	 */
	public int numberOfAttempts() {
		int count = 0;
		
		for ( int i = 0; i < mMask.length; i++) {
			if (mMask[i])
				count++;
		}
		
		return count;
	}
	
	/**
	 * Get the inevitable amount of failure
	 * @return
	 */
	public int numberOfMisses() {
		int count = 0;
		
		for ( int i = 0; i < mMask.length; i++) {
			if (mMask[i]) 
				if (!mField[i])
					count++;
		}
		
		return count;
	}
	
	/**
	 * Get the amount of lucky guesses
	 * @return
	 */
	public int numberOfHits() {
		int count = 0;
		
		for ( int i = 0; i < mMask.length; i++) {
			if (mMask[i]) 
				if (mField[i])
					count++;
		}
		
		return count;
	}
	
	/**
	 * Start the in game countdown timer.
	 */
	public void startTimer() {
		mTimer.start();
	}
	
	/**
	 * Guess what this does...
	 */
	public void stopTimer() {
		mTimer.stop();
	}
	
	/**
	 * Get the number of remaining seconds
	 * @return
	 */
	public int getSeconds() {
		return mSeconds;
	}
	
	/**
	 * This returns a nice string if you wanted to display that kind of thing.
	 * @return
	 */
	public String getDuration() {
		return String.format("%02d:%02d",
				mSeconds / 60,
				mSeconds % 60);
	}

	public int getSize() {
		
		return mSize;
	}
	
}
