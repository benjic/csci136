package com.benjica.csci136.lab4;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * GridPanel is a UI element that renders the current state of the
 * game in a visual way.
 * 
 * Mar 8, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class GridPanel extends JPanel {
	
	// Set sizing
	private static final int SIZE 		= 12;
	private static final int ENEMIES	= 10;
	
	// Sprite files
	private static BufferedImage mHit;
	private static BufferedImage mMiss;
	
	// Game engine
	private KaboomGame mGame;
	private GamePanel mGamePanel;
	private BufferedImage mBackground;
	
	/**
	 * Create new game
	 * @param game
	 * @param statsContainer
	 * @throws IOException
	 */
	public GridPanel(KaboomGame game, GamePanel statsContainer) throws IOException {
		
			// initilize game engine
			mGame = game;
			mGamePanel = statsContainer;
			
			mHit  = ImageIO.read(new File("./img/hit.png"));
			mMiss = ImageIO.read(new File("./img/miss.png"));
			mBackground = ImageIO.read(new File("./img/background.png"));
			
			// Listen for mouse clicks
			this.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					
					// Start the timer if this is the first click
					if (! mGame.mTimer.isRunning() ) {
						mGame.mTimer.start();
					}
					
					// Find where they click
					GridPanel panel = (GridPanel) e.getSource();
					Point p =  e.getPoint();
					
					int w = getWidth();
					int h = getHeight();
					
					int c_w = w / mGame.getSize();
					int c_h = h / mGame.getSize();
					
					// And update the mask
					mGame.getMask()[(( p.x / c_w ) * mGame.getSize()) + p.y / c_h ] = true;
					
					panel.mGamePanel.updateStats();
					panel.repaint();
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					
				}
				
			});
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// Get container
		int w = getWidth();
		int h = getHeight();
		
		
		
		// Build dynamic dimensions
		int c_w = w / mGame.getSize();
		int c_h = h / mGame.getSize();
		int i_x = 0;
		int i_y = 0;
		
		g.drawImage(mBackground,
				0, 0,
				w, h,
				0, 0,
				700, 700, null);
		
		// Iterate over each cell
		for ( int i = 0; i < mGame.getSize()*mGame.getSize(); i++ ) {
			
			// Find corner
			int c_x = i_x + (c_w * (i / mGame.getSize()));
			int c_y = i_y + (c_h * (i % mGame.getSize()));
			
			// Draw depending cell state
			if ( mGame.getMask()[i] ) {
				if (mGame.getField()[i]) { 
					// Draw picture
					g.drawImage(mHit,
						c_x, c_y,
						c_x + c_w, c_y + c_h,
						0, 0,
						75, 75,
						null);
				} else {
					// Draw picture
					g.drawImage(mMiss,
						c_x, c_y,
						c_x + c_w, c_y + c_h,
						0, 0,
						75, 75,
						null);
					
					// Illustrate number of neigbores
					g.setColor(Color.WHITE);
					g.fillRoundRect(c_x+2, c_y+2, 10, 15, 2, 2);
					
					g.setColor(Color.BLACK);
					g.drawString(String.format("%d", mGame.countNeighbores(i)), c_x + 2, c_y + 15);
				}
			} else {
				// Draw an empty square
				g.drawRect(c_x, c_y, 
						c_w, c_h);
			}
		}
		
		
	}

	/**
	 * Reinitialize grid
	 * @param game
	 */
	public void startNewGame(KaboomGame game) {
		mGame = game;
		this.repaint();
	}
}
