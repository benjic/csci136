package com.benjica.csci136.lab4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Hashtable;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.Timer;

/**
 * GamePanel is a container for all the aspects of the game including
 * stats and control. 
 * Mar 8, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class GamePanel extends JPanel {
	
	// Create Labels
	private JLabel mNumberOfAttemptsLabel;
	private JLabel mNumberOfHitsLabel;
	private JLabel mNumberOfMissLabel;
	private JLabel mTimerLabel;
	
	// Create non lable components
	private JButton mNewGameButton;
	private JSlider mDifficultySlider;
	private GridPanel mGridPanel;
	private Timer mTimer;
	
	// A crappy container for highscores
	private int[] mHighScores = new int[3];
	
	// A reference to the game engine
	private KaboomGame mGame;

	/**
	 * Create a new GamePanel
	 * @throws IOException
	 */
	public GamePanel() throws IOException {
		
		// Create a new game and grid panel
		mGame = new KaboomGame(12, 10, 600);
		mGridPanel = new GridPanel(mGame, this);
		
		// Setup labels
		mNumberOfAttemptsLabel = new JLabel("Attemps: 0");
		mNumberOfHitsLabel = new JLabel("Hits: 0");
		mNumberOfMissLabel = new JLabel("Miss: 0");
		mTimerLabel = new JLabel("");
		
		mDifficultySlider = new JSlider(JSlider.HORIZONTAL, 4, 25, 12);
		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put( new Integer( 4 ), new JLabel("Childsplay") );
		labelTable.put( new Integer(25), new JLabel("Masochist"));
		mDifficultySlider.setLabelTable( labelTable );

		mDifficultySlider.setPaintLabels(true);
		
		// Add a button and listener
		mNewGameButton = new JButton("New Game");
		mNewGameButton.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {				
				// Things don't always work out for you...
				try {
					startNewGame();
				} catch (IOException e1) {
					System.err.println("There was a problem loading the sprites.");
				}
				
			}
			
		});
	
		// Initialize the Panel with anew game
		startNewGame();
		
		
		// Use some layout stuff to make it look okay
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setHorizontalGroup(
				layout.createParallelGroup()
					.addComponent(mGridPanel)
					.addGroup(
							layout.createSequentialGroup()
								.addComponent(mNumberOfAttemptsLabel)
								.addComponent(mNumberOfHitsLabel)
								.addComponent(mNumberOfMissLabel)
								.addComponent(mTimerLabel)
								.addComponent(mDifficultySlider)
								.addComponent(mNewGameButton)));
		
		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addComponent(mGridPanel)
					.addGroup(
							layout.createParallelGroup()
								.addComponent(mNumberOfAttemptsLabel)
								.addComponent(mNumberOfHitsLabel)
								.addComponent(mNumberOfMissLabel)
								.addComponent(mTimerLabel)
								.addComponent(mDifficultySlider)
								.addComponent(mNewGameButton)));
	}
	
	/**
	 * This grabs the game and updates the labels with the stats.
	 */
	public void updateStats() {
		mNumberOfAttemptsLabel.setText(String.format("Attempts: %d", mGame.numberOfAttempts()));
		mNumberOfHitsLabel.setText(String.format("Hits: %d", mGame.numberOfHits()));
		mNumberOfMissLabel.setText(String.format("Miss: %d", mGame.numberOfMisses()));
		mTimerLabel.setText("Time Remaining: " + mGame.getDuration());
	}
	
	/**
	 * This manages state continuous games
	 * @throws IOException
	 */
	public void startNewGame() throws IOException {
		
		int difficulty = mDifficultySlider.getValue();
		//Create a new game. The values for the gird 
		mGame = new KaboomGame(
				difficulty,
				(difficulty*difficulty)/10,600);
		
		mGame.mTimer.addActionListener(new ActionListener() {

			/**
			 * The comments don't matter, nobody reads them.
			 *                              -- Michael Cassens CSCI136 
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				updateStats();
				
				try {
				
					
					if (mGame.getSeconds() == 0 ) { // If they ran out of time
						
							// Present a win!
							JOptionPane.showMessageDialog(mGridPanel.getParent().getParent(),
							    "You did not find all of the baddies within the time allotment.",
							    "You have lost!",
							    JOptionPane.ERROR_MESSAGE);
							startNewGame();
							
						
					} else if (mGame.hasWon()) { // If the game has been completed.
						
						// Check for highscore update
						for ( int i = 0; i < 3; i++ ) {
							if ( mHighScores[i] < mGame.numberOfMisses() ) {
								mHighScores[i] = mGame.numberOfMisses();
							}
						}
						
						// Present a win!
						JOptionPane.showMessageDialog(mGridPanel.getParent().getParent(),
							    String.format("You managed to find all the baddies within the time limit! It only took with %d misses!\n\n"
							    		+ "Highscores:\n"
							    		+ "1: %d\n"
							    		+ "2: %d\n"
							    		+ "3: %d\n",
							    		mGame.numberOfAttempts(), mGame.numberOfMisses(),
							    		mHighScores[0],mHighScores[1],mHighScores[2]),
							    "You did it!",
							    JOptionPane.INFORMATION_MESSAGE);
							startNewGame();
	
					}
				} catch (IOException e1) {
					System.err.println("There was a problem loading the sprites.");
				}
				
			}
			
		});
		
		// Updated the panel and stats
		mGridPanel.startNewGame(mGame);
		updateStats();
	}
}
