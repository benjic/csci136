package com.benjica.csci136.hw1;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

import com.benjica.csci136.hw1.pg4.Player;

public class Program4 {
	
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		
		int numPlayers = 0;
		Player p1 = new Player();
		Player p2 = new Player();
		
		while( numPlayers == 0) {
			try {
				
				System.out.printf("Welcome to RPS (console style). "
						+ "Do you want to\n\t1)Play with the computer"
						+ "\n\t2)Play with a friend?\n\n");
				
				numPlayers = Integer.parseInt(input.next("[12]"));
			} catch (InputMismatchException e) {
				System.out.printf("I didn't reconize your input.\n\n");
				input.next();
			}
		}
		
		
		// Add player 1
		addPlayer(p1);
		
		// Test for two players and add a humn or computer
		if (numPlayers > 1) {
			addPlayer(p2);
		} else {
			p2.name = "Johnny #5";
		}
		
		System.out.printf("Alright, here is how to play. Player 1 uses the keys a, s, and d for wizard,\n");
		System.out.printf("goblin, and giant respectively. Simliarily player 2, if playing uses j, k, l repsecitively.");
		System.out.printf("A countdown will begin at the end of which you must play your hand. Do you understand?");
		
		// Just block and let them enter whatever, I don't care if they understand or not.
		input.nextLine();
		
		do {
			
			playRound(p1, p2, (numPlayers == 1));
					
		} while ( playAgain() );
		
		System.out.printf("What a thrilling set of games. Here are your stats.\n\n");
		System.out.println(p1);
		System.out.println(p2);
		

	}
	
	public static void playRound(Player p1, Player p2, boolean computer) {
		
		for ( int i = 3; i > 0; i-- ) {
			System.out.printf("%d!\n", i);
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.printf("GO!\n");
		
		System.out.printf("%s?\n", p1.name);
		
		char p1Input = input.next("[asd]").charAt(0);
		char p2Input = 'j';
		
		System.out.printf("%s?\n", p2.name);
		
		if ( computer ) {
			
			Random rand = new Random();
			
			switch( rand.nextInt() % 3 ) {
			case 0:
				p2Input = 'j';
				break;
			case 1:
				p2Input = 'k';
				break;
			case 2:
				p2Input = 'l';
				break;
			}
			
			System.out.printf("%c\n", p2Input);
						 
		} else {
			p2Input = input.next("[jkl]").charAt(0);
		}
		
		
		switch(p1Input) {
		case 'a':
			if ( p2Input == 'k') {
				System.out.printf("%s Wins!\n", p1.name);
				p1.wins++;
				p2.loses++;
			} else if (p2Input == 'l') {
				System.out.printf("%s Wins!\n", p2.name);
				p2.wins++;
				p1.loses++;
			} else {
				System.out.printf("Nobody wins");
				p1.draws++;
				p2.draws++;
			}
			break;
		case 's':
			if ( p2Input == 'l') {
				System.out.printf("%s Wins!\n", p1.name);
				p1.wins++;
				p2.loses++;
			} else if (p2Input == 'j') {
				System.out.printf("%s Wins!\n", p2.name);
				p2.wins++;
				p1.loses++;
			} else {
				System.out.printf("Nobody wins");
				p1.draws++;
				p2.draws++;
			}
			break;
			
		case 'd':
			if ( p2Input == 'j') {
				System.out.printf("%s Wins!\n", p1.name);
				p1.wins++;
				p2.loses++;
			} else if (p2Input == 'k') {
				System.out.printf("%s Wins!\n", p2.name);
				p2.wins++;
				p1.loses++;
			} else {
				System.out.printf("Nobody wins");
				p1.draws++;
				p2.draws++;
			}
			break;
		}
	}
	
	public static void addPlayer(Player p) {
		
		while( p.name.equals("") ) {
			
			try {
				System.out.printf("Please enter your name:\n");
				
				input.nextLine();
				p.name = input.nextLine();
			} catch (InputMismatchException e) {
				System.out.printf("I didn't reconize your input.\n\n");
				input.next();
			}
		}
	}

	public static boolean playAgain() {
		
		System.out.printf("Would you like to play again?[yn]\n");
		String again = "";
		
		do {
			
			try {
				again = input.next("[YyNn]");
			} catch (InputMismatchException e) {
				System.out.printf("I didn't understand your input.\n\n");
				input.nextLine();
			}
			
		} while ( again.matches("[YyNn].") );
		
		return again.toLowerCase().equals("y");
			
	}
}
