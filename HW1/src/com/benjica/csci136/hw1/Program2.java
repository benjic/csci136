package com.benjica.csci136.hw1;

import com.benjica.csci136.hw1.pg2.DriversLicenseJFrame;

/**
 * This is a driver class for Program 2.
 * @author benjica
 *
 */
public class Program2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DriversLicenseJFrame dl = new DriversLicenseJFrame();
		
		dl.pack();
		dl.setVisible(true);
	}

}
