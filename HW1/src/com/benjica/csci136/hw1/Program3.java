package com.benjica.csci136.hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.benjica.csci136.hw1.pg3.Phone;

public class Program3 {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner fScanner = new Scanner(new File("./data/pg3/phones.csv"));
		Phone[] phones = new Phone[5];
		int i = 0;
		
		while ( fScanner.hasNextLine() && i < 5) {
			String[] params = fScanner.nextLine().split(",");
			
			phones[i] = new Phone(
								params[0],
								Integer.parseInt(params[1]),
								Integer.parseInt(params[2]),
								params[3],
								params[4]);
			i++;
		}
		
		for( Phone p : phones ) {
			System.out.println(p);
		}

	}

}
