package com.benjica.csci136.hw1.pg3;

public class Phone {
	
	private String mProcessor;
	private int mRAM;
	private int mFlash;
	private String mOS;
	private String mScreenSize;
	
	public Phone() {
		mProcessor = "None";
		mRAM = 0;
		mFlash = 0;
		mOS = "None";
		mScreenSize = "0x0";
	}
	
	public Phone(String processor, int ram, int flash, String os, String screen) {
		mProcessor = processor;
		mRAM = ram;
		mFlash = flash;
		mOS = os;
		mScreenSize = screen;
	}
	
	public String toString() {
		return String.format(
				"This phone has a %s processor and runs the %s OS. It has %d RAM and %d flash space and a screen size of %s", 
				mProcessor, mOS, mRAM, mFlash, mScreenSize);
	}

	public String getmProcessor() {
		return mProcessor;
	}

	public void setmProcessor(String mProcessor) {
		this.mProcessor = mProcessor;
	}

	public int getmRAM() {
		return mRAM;
	}

	public void setmRAM(int mRAM) {
		this.mRAM = mRAM;
	}

	public int getmFlash() {
		return mFlash;
	}

	public void setmFlash(int mFlash) {
		this.mFlash = mFlash;
	}

	public String getmOS() {
		return mOS;
	}

	public void setmOS(String mOS) {
		this.mOS = mOS;
	}

	public String getmScreenSize() {
		return mScreenSize;
	}

	public void setmScreenSize(String mScreenSize) {
		this.mScreenSize = mScreenSize;
	}
}
