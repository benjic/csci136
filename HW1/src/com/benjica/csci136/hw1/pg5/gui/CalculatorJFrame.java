package com.benjica.csci136.hw1.pg5.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.benjica.csci136.hw1.pg5.Expression;


public class CalculatorJFrame extends JFrame {
	
	// Utility components
	private KeypadPanel kp;
	private JTextArea inputLabel;
	private JButton clearButton;
	private JButton equalButton;
	
	public CalculatorJFrame() {
		
		// Make a nice new title
		setTitle("Magic Calculator");
		
		// Setup main panel with layout
		JPanel mainpanel = new JPanel();
		GroupLayout layout = new GroupLayout(mainpanel);
		mainpanel.setLayout(layout);
		
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		// Instantiate our components
		clearButton = new JButton("Clear");
		equalButton = new JButton("Compute");
		
		inputLabel = new JTextArea();
		inputLabel.setBorder( BorderFactory.createLineBorder( new Color(0,0,0)));
		
		kp = new KeypadPanel(inputLabel);
		
		// Setup clear button to wipe inputlabel
		clearButton.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Just reset text
				inputLabel.setText("");
				
			}
			
		});
		
		// Setup clear button to wipe inputlabel
		equalButton.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Just reset text
				
				Expression exp = new Expression(inputLabel.getText());
				
				inputLabel.setText(String.format("%f", exp.getValue()));
				
			}
			
		});
				
		// Layout group 
		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addComponent(inputLabel)
					.addGroup(
						layout.createParallelGroup()
							.addGroup(
									layout.createSequentialGroup()
										.addComponent(clearButton)
										.addComponent(equalButton))
							.addComponent(kp))
				);
		
		layout.setHorizontalGroup(
				layout.createParallelGroup()
					.addComponent(inputLabel)
					.addGroup(
							layout.createSequentialGroup()
								.addComponent(kp)
								.addGroup(
										layout.createParallelGroup()
											.addComponent(clearButton)
											.addComponent(equalButton)
								)
					)
				);
		
		// Add panel to frame
		add(mainpanel);
	}
}
