package com.benjica.csci136.hw1.pg5.gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class KeypadPanel extends JPanel {
	
	// Class Constants
	private static final String[] BUTTONS = {"1", "2", "3", " + ", "4", "5", "6", " - ", "7", "8",
												"9", " * ", "0", ".", " % ", " / "};
	// Class fields
	JTextArea mInputLabel;
	
	public KeypadPanel(JTextArea inputLabel) {
		
		// Set inputlabel to outside label
		mInputLabel = inputLabel;
		
		// Make a nice grid layout
		GridLayout layout = new GridLayout(0,4);
		setLayout(layout);
		
		// Gap the buttons
		layout.setHgap(2);
		layout.setVgap(2);
		
		// Iterate over BUTTON types and create buttons 
		for(String labelString : BUTTONS) {
			
			JButton button = new JButton(labelString);
			button.addActionListener( new ButtonActionListener());
			
			add(button);
		}
		
		// Give this a pretty border
		this.setBorder(BorderFactory.createLineBorder( new Color(0,0,0)));
	}
	
	private class ButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// Grab the button that fired event
			JButton callingButton = (JButton) e.getSource();
			
			// Concat the button label to input label
			mInputLabel.setText( mInputLabel.getText() + callingButton.getText());
			
		}
		
	}
}
