package com.benjica.csci136.hw1.pg5;

public class Value {
	private double value;

	public Value() {
		value = 0;
	}
	
	public Value(String v) {
		this.value = Double.parseDouble(v);
	}
	
	public double getValue() {
		return value;
	}
}
