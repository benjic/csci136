package com.benjica.csci136.hw1.pg5;


public class Expression extends Value {
	
	private Value op1;
	private char operand;
	private Value op2;
	
	
	public Expression(String exp) {
		String[] params = exp.split(" ");
				
		op1 = new Value(params[0]);
		operand = params[1].charAt(0);
		
		if ( (params.length - 2) > 1 ) {
			op2 = new Expression(implode(params));
		} else {
			op2 = new Value(params[params.length-1]);
		}
	}
	
	public double getValue() {
				
		switch ( operand ) {
		case '+':
			return op1.getValue() + op2.getValue();
		case '-':
			return op1.getValue() - op2.getValue();
		case '*':
			return op1.getValue() * op2.getValue();
		case '/':
			return op1.getValue() / op2.getValue();
		case '%':
			return op1.getValue() % op2.getValue();
		}
		
		return 0;
	}
	
	private String implode(String[] ar) {
		String expr = "";
		
		for ( int i = 2; i < ar.length; i++ ) {
			expr += ar[i] + " ";
		}
			
		return expr.trim();
	}
}

