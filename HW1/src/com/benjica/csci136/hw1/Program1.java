package com.benjica.csci136.hw1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This is a driver class for program 1.
 * @author benjica <bc223913@umconnect.umt.edu>
 *
 */
public class Program1 {

	final static double EARTH_RADIUS = 6371 * 1000; 	// Earths radius in meters
	
	final static double YANKEE_CYLINDER_RADIUS = 3.35; 	// radius in meters
	final static double YANKEE_CYLINDER_HEIGHT = 5.4;  	// height in meters
	
	/*
	 * I couldn't find the worlds largest regular polygon shaped pool.
	 * So I made this one up.
	 */
	final static double SWIMMING_POOL_WIDTH = 100; 		// width in meters
	final static double SWIMMING_POOL_LENGTH = 1000; 	// length in meters
	final static double SWIMMING_POOL_DEPTH = 25;  		// Depth in meters
	
	/**
	 * The main method.
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner inputScanner = new Scanner(System.in);
		
		String name = "";
		String property = "";
		String units = "";
		double value = 0;
		
		while (name.equals("")) {
			
			System.out.printf("What object below are you most interested in?\n");
			System.out.printf("\t1) Earth\n");
			System.out.printf("\t2) Yankee Cylinders\n");
			System.out.printf("\t3) Swimming pools\n\n");
			
			try {
				
				switch( Integer.parseInt( inputScanner.next("[123]") ) ) {
				case 1:	
					name = "Earth";
					break;
				case 2:
					name = "worlds largest yankee cylinder";
					break;
				case 3:
					name = "worlds largest swimming pool";
					break;
				}
				
			} catch (InputMismatchException e) {
				System.out.printf("I didn't reconize your input. Try again.\n\n");
				inputScanner.next();
			}
			
		}
		
		while (property.equals("")) {
			
			System.out.printf("What metric below are you most interested in?\n");
			System.out.printf("\t1) Surface Area\n");
			System.out.printf("\t2) Volume\n");
			
			try {
				switch( Integer.parseInt( inputScanner.next("[12]") ) ) {
				case 1:
					property = "surface area";
					units = "m^2";
					break;
				case 2:
					property = "volume";
					units = "m^3";
					break;
				}
			} catch (InputMismatchException e) {
				System.out.printf("I didn't reconize your input. Try again.\n\n");
				inputScanner.next();
			}
			
		}
		
		inputScanner.close();
		
		switch ( name ) {
		case "Earth":
			value = ( property.equals("surface area") ) ? surfaceArea(EARTH_RADIUS) : volume(EARTH_RADIUS);
			break;
		case "worlds largest yankee cylinder":
			value = ( property.equals("surface area") ) ? surfaceArea(YANKEE_CYLINDER_RADIUS, YANKEE_CYLINDER_HEIGHT) : volume(YANKEE_CYLINDER_RADIUS, YANKEE_CYLINDER_HEIGHT);
			break;
		case "worlds largest swimming pool":
			value = ( property.equals("surface area") ) ? surfaceArea(SWIMMING_POOL_WIDTH, SWIMMING_POOL_LENGTH, SWIMMING_POOL_DEPTH) : volume(SWIMMING_POOL_WIDTH, SWIMMING_POOL_LENGTH, SWIMMING_POOL_DEPTH);
			break;
		}
		
		System.out.printf("The %s of %s is %f %s.\n", property, name, value, units);
		
	}
	
	/**
	 * This is the surface area method for a sphere.
	 * @param radius
	 * @return The surface area of a sphere of radius
	 */
	public static double surfaceArea(double radius) {
		
		return 4 * Math.PI * Math.pow(radius, 2);
	}
	
	/**
	 * 
	 * A function to calculate the surface area of a cylinder.
	 * 
	 * @param radius
	 * @param height
	 * @return The surface area of a cylinder of height and radius.
	 */
	public static double surfaceArea(double radius, double height) {
		
		return (2 * Math.PI *  Math.pow(radius, 2)) 
				+ (2 * Math.PI * radius * height);
	}
	
	/**
	 * 
	 * A function to calculate the surface area of a prism.
	 * 
	 * @param width
	 * @param height
	 * @param depth
	 * @return
	 */
	public static double surfaceArea(double width, double height, double depth) {
		
		return (2 * width * height)
				+ (2 * height * depth)
				+ (2 * width  * depth);
	}
	
	/**
	 * A function to calculate the volume of a sphere.
	 * @param radius
	 * @return Volume
	 */
	public static double volume(double radius) {
		
		return (4.0/3.0) * Math.PI * Math.pow(radius, 2);
		
	}
	
	/**
	 * 
	 * A function to calculate the volume of a cylinder.
	 * @param radius
	 * @param height
	 * @return volume
	 */
	public static double volume(double radius, double height) {
		
		return 2 * Math.PI * Math.pow(radius, 2) * height;
		
	}
	
	/**
	 * 
	 * A function to calculate the volume of a prism.
	 * 
	 * @param width
	 * @param height
	 * @param depth
	 * @return
	 */
	public static double volume(double width, double height, double depth) {
		
		return width * height * depth;
		
	}

}
