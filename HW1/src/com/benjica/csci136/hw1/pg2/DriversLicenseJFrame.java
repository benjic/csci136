package com.benjica.csci136.hw1.pg2;

import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * A class to represent the main licsense applicaiton.
 * @author benjica
 *
 */
public class DriversLicenseJFrame extends JFrame {

	public DriversLicenseJFrame() {

		setTitle("Drivers License Form");
		setPreferredSize(new Dimension(600,350));
		
		add(new DriversLicenseJPanel());
	}
}
