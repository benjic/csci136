package com.benjica.csci136.hw1.pg2;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Random;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A class designed to hold the drives license panel.
 * @author benjica
 *
 */
public class DriversLicenseJPanel extends JPanel {
	
	private JLabel mLastNameJLabel;
	private JLabel mFirstNameJLabel;
	private JTextField mFirstNameJTextField;
	private JTextField mLastNameJTextField;
	private JButton mSubmitJButton;
	private JLabel mImageLabel;
	private JLabel mIDLabel;
	
	private Random rand = new Random();

	public DriversLicenseJPanel() {
		
		mFirstNameJLabel 		= new JLabel("First Name");
		mLastNameJLabel 		= new JLabel("Last Name");
		mImageLabel				= new JLabel();
		mIDLabel				= new JLabel();
		
		mFirstNameJTextField 	= new JTextField(25);
		mLastNameJTextField		= new JTextField(25);
		
		mSubmitJButton			= new JButton("Submit Applicaiton");
		
		mSubmitJButton.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mIDLabel.setText("ID: " + String.valueOf(rand.nextInt(1000000000 - 1) + 1000000000));
				mImageLabel.setIcon(new ImageIcon("./data/pg2/imgs/" + (rand.nextInt(4) + 1) + ".jpg"));
				
			}
			
		});
		
		GroupLayout layout = new GroupLayout(this);
		this.setLayout(layout);
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setHorizontalGroup(
				layout.createSequentialGroup()
					.addGroup(
							layout.createParallelGroup(Alignment.TRAILING)
							.addGroup(
									layout.createSequentialGroup()
									.addComponent(mFirstNameJLabel)
									.addComponent(mFirstNameJTextField))
							.addGroup(
									layout.createSequentialGroup()
									.addComponent(mLastNameJLabel)
									.addComponent(mLastNameJTextField)
					
									).addComponent(mSubmitJButton)
					).addGroup(
							layout.createParallelGroup()
								.addComponent(mIDLabel)
								.addComponent(mImageLabel)
					)
		);
		
		layout.setVerticalGroup(
				layout.createParallelGroup()
					.addGroup(
						layout.createSequentialGroup()
							.addGroup(
									layout.createParallelGroup(Alignment.BASELINE)
										.addComponent(mFirstNameJLabel)
										.addComponent(mFirstNameJTextField))
							.addGroup(
									layout.createParallelGroup(Alignment.BASELINE)
									.addComponent(mLastNameJLabel)
									.addComponent(mLastNameJTextField)
							).addComponent(mSubmitJButton)
					).addGroup(
							layout.createSequentialGroup()
								.addComponent(mIDLabel)
								.addComponent(mImageLabel)
					)
					
		);
		
	}

}
