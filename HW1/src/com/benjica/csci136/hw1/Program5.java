package com.benjica.csci136.hw1;

import com.benjica.csci136.hw1.pg5.Expression;
import com.benjica.csci136.hw1.pg5.gui.CalculatorJFrame;

public class Program5 {

	public static void main(String[] args) {

		CalculatorJFrame calc = new CalculatorJFrame();
		
		calc.pack();
		calc.setVisible(true);
	}

}
