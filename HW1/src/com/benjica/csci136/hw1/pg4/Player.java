package com.benjica.csci136.hw1.pg4;

public class Player {
	
	public String name = "";
	public int wins  = 0;
	public int draws = 0;
	public int loses = 0;
	
	public String toString() {
		return String.format("%s: %3dw %3dd %3dl",
								name,
								wins,
								draws,
								loses);
	}

}
