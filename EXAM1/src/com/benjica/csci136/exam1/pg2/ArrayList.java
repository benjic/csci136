package com.benjica.csci136.exam1.pg2;

/**
 * My implementation of array lists. I was going to implement List but there is like
 * fifty methods in that interface and it just seemed like it was way more work then is
 * required for *your* interface. I promise if this were production code I would implement
 * the full interface instead of this facade.
 * 
 * Mar 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class ArrayList<T> {

	// No magic numbers
	private static final int INITIAL_SIZE = 25;
	private static final int GROWTH_CONSTANT = 2;
	
	// Class members
	private Object[] mContainer;
	private int mIndex;
	
	// Constructor
	public ArrayList() {
		mContainer = new Object[INITIAL_SIZE];
		mIndex = 0;
	}
	
	/**
	 * This method is used resize the container by creating a new array and copying existing 
	 * data into the new array and changing the member reference.
	 */
	private void resizeContainer() {
		
		Object[] newArray = new Object[mContainer.length * GROWTH_CONSTANT];
		
		for (int i = 0; i < mContainer.length; i++) {
			newArray[i] = mContainer[i];
		}
		
		mContainer = newArray;
	}

	/**
	 * This adds an element to the list
	 * @param e Element
	 * @return Success?
	 */
	public boolean add(T e) {
		// I don't know if the array is ready so I'll try
		try {
			mContainer[mIndex] = e;
			mIndex++;
		} catch (ArrayIndexOutOfBoundsException ex) {
			// If the container can't take it any more make it bigger and readd.
			resizeContainer();
			add(e);
		}
		
		return true;
	}

	/**
	 * This grabs the ith element.
	 * @param index
	 * @return
	 */
	public T get(int index) {
		return (T) mContainer[index];
	}
	
	/**
	 * Why not the size of the array?
	 * @return
	 */
	public int size() {
		return mIndex;
	}
	
}
