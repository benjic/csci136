package com.benjica.csci136.exam1.pg2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.benjica.csci136.exam1.pg1.Event;

/**
 * TODO Implement Class Definition for Program2.java Mar 7, 2014
 * 
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program2 {

	public static void main(String[] args) {
		// Initialize Collections
		ArrayList<CreditCard> compliants = new ArrayList<CreditCard>();
		// Check for right number of parameters
		if (args.length > 0) {

			// Try to parse
			try {

				// Open utility classes
				Scanner fileScanner = new Scanner(new File(args[0]));

				// Grab initial time
				long start = System.nanoTime();

				// Iterate over lines
				while (fileScanner.hasNextLine()) {
					// Blow up line and put data in structure
					String[] params = fileScanner.nextLine().split(",");
					
					compliants.add(new CreditCard(params[0], params[3], params[6]));

				}

				// Clean up
				fileScanner.close();
				long stop = System.nanoTime();
				
				System.out.printf("Finished processing %d events in %f seconds.\n",
						compliants.size(), 						// The total number of records
						((stop - start) / 1000000000.0));	// The elapsed time in seconds.
				System.out.printf("+------------------------------------------------------------------------------+\n");
				System.out.printf("| %10s | %50s | %6s |\n",
						"IssueID",
						"Issue",
						"Zip");
						
				System.out.printf("+------------------------------------------------------------------------------+\n");

				for ( int i = 0; i < compliants.size(); i++ ) {
					CreditCard c = compliants.get(i);
					System.out.printf("| %10s | %50s | %6s |\n",
							c.CompliantID,
							c.Issue,
							c.Zipcode);
				}
				
				System.out.printf("+------------------------------------------------------------------------------+\n");

			} catch (FileNotFoundException e) {
				// Sometimes your param is just wrong
				System.err.printf("The file %s was not found.", args[0]);
			}
		} else {
			// Let the user know how to run the program.
			System.out
					.printf("Program2 <data file>\n You have not specified a input file.");
		}

	}

}
