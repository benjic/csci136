package com.benjica.csci136.exam1.pg2;

/**
 * A simple data structure for credit card complaints.
 * Mar 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class CreditCard {
	
	String CompliantID;
	String Issue;
	String Zipcode;
	
	public CreditCard(String complaint_id, String issue, String zipcode) {
		
		CompliantID = complaint_id;
		Issue = issue;
		Zipcode = zipcode;
		
	}
}
