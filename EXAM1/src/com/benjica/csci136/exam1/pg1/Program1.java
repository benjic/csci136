package com.benjica.csci136.exam1.pg1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * The main driver for program 1 which loads the given file and
 * parses the data.
 * Mar 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program1 {

	public static void main(String[] args) {
		
		// Initialize Collections
		Map<String, Map<String, Integer>> states = new HashMap<String, Map<String, Integer>>();
		List<Event> events = new ArrayList<Event>();
		
		// Check for right number of parameters
		if ( args.length > 0) {
				
			// Try to parse
				try {
					
					// Open utility classes
					Scanner fileScanner = new Scanner(new File(args[0]));
					
					// Grab intial time
					long start = System.nanoTime();
					
					// Iterate over lines
					while ( fileScanner.hasNextLine() ) {
						// Blow up line and put data in structure
						String[] params = fileScanner.nextLine().split(",");
						Event e = new Event(params[8], params[10], params[12]);
						
						// Add the state to the map if it is the first instance
						if (!states.containsKey(params[8]))
							states.put(params[8], new HashMap<String,Integer>());
						
						// Grab the state map from the map
						Map<String, Integer> state = states.get(params[8]);
						
						// Test if the event is in the state map
						if (!state.containsKey(params[12])) {
							// Add the event and value of 1 if first instance
							state.put(params[12], 1);
						} else {
							// Or increment existing events
							state.put(params[12], state.get(params[12]) + 1);
						}
						
						// Add event to list collection as well.
						events.add(e);
					}
					
					// Clean up
					fileScanner.close();
					long stop = System.nanoTime();
					
					// Print Success
					System.out.printf("Finished processing %d events in %f seconds.\n",
								events.size(), 						// The total number of records
								((stop - start) / 1000000000.0));	// The elapsed time in seconds.
					
					System.out.printf("Below is a summary of events listed by state.\n");
					
					// Iterate over each state key
					for(String state : states.keySet()) {
						System.out.printf("%s:\n", state);
						
						// Iterate over each event
						for ( String event : states.get(state).keySet() ) {
							System.out.printf("   %25s : %10d\n", event, states.get(state).get(event));
						}
					}
					
				} catch (FileNotFoundException e) {
					// Sometimes your param is just wrong
					System.err.printf("The file %s was not found.", args[0]);
				}
		} else {
			// Let the user know how to run the program.
			System.out.printf("Program1 <data file>\n You have not specified a input file.");
		}
		
	}
	
}
