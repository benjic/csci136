package com.benjica.csci136.exam1.pg1;
/**
 * A data structure for events
 * Mar 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Event {
	public String mState;
	public String mDate;
	public String mType;
	
	public Event(String state, String date, String type) {
		mState = state;
		mDate  = date;
		mType  = type;
	}
}
