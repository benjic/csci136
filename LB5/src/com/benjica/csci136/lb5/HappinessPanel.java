package com.benjica.csci136.lb5;

import java.awt.Dimension;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.benjica.csci136.lb5.simu.LifeEngine;
import com.benjica.csci136.lb5.simu.Person;

/**
 * TODO Implement Class Definition for HappinessPanel.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class HappinessPanel extends JPanel {
	
	public HappinessPanel(LifeEngine le) {
		
		JList<Person> list = new JList<Person>(le.getHousehold());
		list.setCellRenderer(new PersonListRenderer());
		
		JScrollPane sp = new JScrollPane(list);
		
		add(sp);
	}
}
