package com.benjica.csci136.lb5;

import java.awt.Component;
import java.io.Serializable;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.ListCellRenderer;

import com.benjica.csci136.lb5.simu.Person;

/**
 * TODO Implement Class Definition for PersonListRenderer.java
 * Apr 24, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class PersonListRenderer implements Serializable, ListCellRenderer<Person> {

	@Override
	public Component getListCellRendererComponent(JList<? extends Person> list,
			Person value, int index, boolean isSelected, boolean cellHasFocus) {
		
		JPanel personPanel = new JPanel();
		
		JLabel personLabel = new JLabel(value.getName());
		personLabel.setIcon(new ImageIcon("data/imgs/user.png"));
		
		StringBuilder sb = new StringBuilder();
		for (String s : value.getThingsThatMakeMeHappy()) {
			sb.append(s + ", ");
		}
		
		JLabel statsLabel = new JLabel(sb.toString());
		
		JProgressBar happinessProgress = new JProgressBar(0, 100);
		happinessProgress.setValue((int) value.getHappiness());
		
		personPanel.add(personLabel);
		personPanel.add(statsLabel);
		personPanel.add(happinessProgress);
		
		return personPanel;
	}	
}