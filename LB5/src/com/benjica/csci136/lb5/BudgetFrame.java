package com.benjica.csci136.lb5;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.benjica.csci136.lb5.simu.LifeEngine;

/**
 * TODO Implement Class Definition for BudgetFrame.java
 * Apr 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class BudgetFrame extends JFrame  {
	
	private BudgetPanel mBudgetPanel;
	
	
	private LifeEngine mLifeEngine;
	
	public BudgetFrame(LifeEngine le) {
		
		mLifeEngine = le;
		
		mBudgetPanel = new BudgetPanel(le);
		
				
		add(mBudgetPanel);
		
	}

}
