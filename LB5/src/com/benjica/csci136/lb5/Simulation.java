package com.benjica.csci136.lb5;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.benjica.csci136.lb5.simu.LifeEngine;

/**
 * TODO Implement Class Definition for Simulation.java Mar 19, 2014
 * 
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Simulation extends JFrame implements ActionListener {

	private JButton mNewButton = new JButton("New Game");
	private JButton mLoadButton = new JButton("Load Game");
	private JButton mSaveButton = new JButton("Save Game");

	private JPanel mStartPanel;

	private GroupLayout mLayout;
	private LifeEngine mLifeEngine;
	private BudgetFrame mBudgetFrame;
	private HappinessFrame mHappinessFrame;

	public Simulation() {

		mStartPanel = new JPanel();

		mLayout = new GroupLayout(mStartPanel);
		mStartPanel.setLayout(mLayout);

		mLayout.setAutoCreateContainerGaps(true);
		mLayout.setAutoCreateGaps(true);

		mNewButton.addActionListener(this);
		mLoadButton.addActionListener(this);
		mSaveButton.addActionListener(this);

		mLayout.setHorizontalGroup(mLayout.createParallelGroup()
				.addComponent(mNewButton).addComponent(mLoadButton)
				.addComponent(mSaveButton));

		mLayout.setVerticalGroup(mLayout.createSequentialGroup()
				.addComponent(mNewButton).addComponent(mLoadButton)
				.addComponent(mSaveButton));

		add(mStartPanel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		if (e.getSource() == mNewButton) {

			mNewButton.setEnabled(false);
			mLoadButton.setEnabled(false);

			mLifeEngine = new LifeEngine();
			mBudgetFrame = new BudgetFrame(mLifeEngine);
			mHappinessFrame = new HappinessFrame(mLifeEngine);

			mBudgetFrame.pack();
			mBudgetFrame.setVisible(true);

			mHappinessFrame.pack();
			mHappinessFrame.setVisible(true);

			mLifeEngine.start();
		} else if (e.getSource() == mLoadButton) {

			if (fc.showOpenDialog(this) == fc.APPROVE_OPTION) {

				String base = fc.getSelectedFile().getAbsolutePath();

				try {
					FileInputStream fileIn = new FileInputStream(base
							+ "/le.ser");
					ObjectInputStream in = new ObjectInputStream(fileIn);
					mLifeEngine = (LifeEngine) in.readObject();
					in.close();
					fileIn.close();


					mBudgetFrame = new BudgetFrame(mLifeEngine);
					mHappinessFrame = new HappinessFrame(mLifeEngine);

					mBudgetFrame.pack();
					mBudgetFrame.setVisible(true);

					mHappinessFrame.pack();
					mHappinessFrame.setVisible(true);

					mLifeEngine.start();

					mNewButton.setEnabled(false);
					mLoadButton.setEnabled(false);

				} catch (IOException ex) {
					System.err.printf("There was an error opening your file.");
					
					ex.printStackTrace();
				} catch (ClassNotFoundException e1) {
					System.err
							.printf("The given class was not found in your jvm.");
				}
			}
		} else {

			if (fc.showOpenDialog(this) == fc.APPROVE_OPTION) {

				String base = fc.getSelectedFile().getAbsolutePath();

				try {
					FileOutputStream fileOut = new FileOutputStream(base
							+ "/le.ser");
					ObjectOutputStream out = new ObjectOutputStream(fileOut);
					out.writeObject(mLifeEngine);
					out.close();
					fileOut.close();

				} catch (IOException ex) {
					System.err.printf("There was an error saving your file.\n");
					ex.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {

		Simulation s = new Simulation();

		s.pack();
		s.setVisible(true);

	}
}
