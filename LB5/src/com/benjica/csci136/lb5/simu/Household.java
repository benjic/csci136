package com.benjica.csci136.lb5.simu;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * TODO Implement Class Definition for Household.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Household extends ArrayList<Person> implements Serializable, ListModel<Person> {
	
	transient private ArrayList<ListDataListener> mListDataListeners;
	private Ledger mLedger;
	
	public Household() {
		mListDataListeners = new ArrayList<ListDataListener>();
		mLedger = new Ledger();
	}
	
	public Ledger getLedger() {
		return mLedger;
	}

	@Override
	public void addListDataListener(ListDataListener arg0) {
		mListDataListeners.add(arg0);
	}

	@Override
	public Person getElementAt(int arg0) {
		// TODO Auto-generated method stub
		return get(arg0);
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return size();
	}

	@Override
	public void removeListDataListener(ListDataListener arg0) {
		mListDataListeners.remove(arg0);
	}

	public void update(Person p) {
		
		for( ListDataListener l : mListDataListeners ) {
			l.contentsChanged(new ListDataEvent(p, ListDataEvent.CONTENTS_CHANGED, 0, 0));
		}
		
	}

	public void reset() {
		mLedger = new Ledger();
	}
	
	private void readObject(ObjectInputStream in) 
			throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		mListDataListeners = new ArrayList<ListDataListener>();
	}
}
