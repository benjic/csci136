package com.benjica.csci136.lb5.simu;

import java.io.Serializable;

/**
 * TODO Implement Class Definition for LineItem.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class LineItem implements Serializable {
	private int mDate;
	private String mDescription;
	private double mAmount;
	
	public LineItem(int i, String desc, double amount) {
		mDate = i;
		mDescription = desc;
		mAmount = amount;
	}

	public String getDescription() {
		return mDescription;
	}

	public double getAmount() {
		return mAmount;
	}
	
	public int getDate() {
		return mDate;
	}
}
