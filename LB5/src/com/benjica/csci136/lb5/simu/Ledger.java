package com.benjica.csci136.lb5.simu;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.event.ListDataListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * TODO Implement Class Definition for Ledger.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Ledger extends ArrayList<LineItem> implements TableModel {
	
	private NumberFormat cf;
	transient private ArrayList<TableModelListener> mTableModelListeners;
	
	public Ledger() {
		mTableModelListeners = new ArrayList<TableModelListener>();
		cf = NumberFormat.getCurrencyInstance();
		add(new LineItem(0, "Initial Deposit", 10000));
	}
	
	
	
	@Override
	public boolean add(LineItem e) {
		
		for(TableModelListener l : mTableModelListeners) {
			l.tableChanged(new TableModelEvent(this));
		}
		
		return super.add(e);
	}



	public double getCurrentBalance() {
		
		return getBalanceOver(0, size());
	}
	
	private double getBalanceOver(int i, int j) {
		
		double v = 0;
		
		for(LineItem l : this.subList(i, j)) {
			v += l.getAmount();
		}
		
		return v;
	}

	@Override
	public void addTableModelListener(TableModelListener arg0) {
		mTableModelListeners.add(arg0);
	}

	@Override
	public Class<?> getColumnClass(int arg0) {
		if ( arg0 == 0)
			return Integer.class;
		else
			return String.class;
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int arg0) {
		switch ( arg0 ) {
		case 0:
			return "Date";
		case 1:
			return "Description";
		case 2:
			return "Amount";
		default:
			return "Balance";
		}
	}

	@Override
	public int getRowCount() {
		return size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1) {
		LineItem l = get(arg0);
		
		switch ( arg1 ) {
		case 0:
			return l.getDate();
		case 1:
			return l.getDescription();
		case 2:
			return cf.format(l.getAmount());
		default:
			return cf.format( this.getBalanceOver(0, arg0+1));
		}
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		mTableModelListeners.remove(arg0);
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// Noop
		
	}
	
	private void readObject(ObjectInputStream in) 
			throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		mTableModelListeners = new ArrayList<TableModelListener>();
	}
}
