package com.benjica.csci136.lb5.simu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * TODO Implement Class Definition for Person.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public abstract class Person implements Serializable, ActionListener {

	private double mHappiness = 100;
	private String mName;
	
	private double[] mPersonalityCoef = new double[8];
	private double[] mHappinessRates = new double[8];
	
	private Household mHouseHold;
	
	public Person(String name, LifeEngine le) {
		
		Random rand = new Random();
		
		mName = name;
		
		le.addActionListener(this);
		mHouseHold = le.getHousehold();
		
		for (int i = 0; i < mPersonalityCoef.length; i++ ) {
			
			if (rand.nextBoolean())
				mPersonalityCoef[i] = rand.nextDouble();
			else
				mPersonalityCoef[i]	= -1 * rand.nextDouble();
			
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		mHappinessRates = new double[8];
		if ( mHouseHold.size() > 30 ) {
			for (LineItem l : mHouseHold.getLedger().subList(mHouseHold.getLedger().size() - 30, mHouseHold.getLedger().size())) {
				switch ( l.getDescription() ) {
				case "Rent Payment":
					mHappinessRates[0] += mPersonalityCoef[0];
					break;
				case "Utility Payment":
					mHappinessRates[1] += mPersonalityCoef[1];
					break;
				case "Groceries Payment":
					mHappinessRates[2] += mPersonalityCoef[2];
					break;
				case "Taxes Payment":
					mHappinessRates[3] += mPersonalityCoef[3];
					break;
				case "Car Payment":
					mHappinessRates[4] += mPersonalityCoef[4];
					break;
				case "Insurance Payment":
					mHappinessRates[5] += mPersonalityCoef[5];
					break;
				case "Savings Payment":
					mHappinessRates[6] += mPersonalityCoef[6];
					break;
				case "Entertainment Payment":
					mHappinessRates[7] += mPersonalityCoef[7];
					break;
					
				}
			}
		} else {
			for (LineItem l : mHouseHold.getLedger()) {
				switch ( l.getDescription() ) {
				case "Rent Payment":
					mHappinessRates[0] += mPersonalityCoef[0];
					break;
				case "Utility Payment":
					mHappinessRates[1] += mPersonalityCoef[1];
					break;
				case "Groceries Payment":
					mHappinessRates[2] += mPersonalityCoef[2];
					break;
				case "Taxes Payment":
					mHappinessRates[3] += mPersonalityCoef[3];
					break;
				case "Car Payment":
					mHappinessRates[4] += mPersonalityCoef[4];
					break;
				case "Insurance Payment":
					mHappinessRates[5] += mPersonalityCoef[5];
					break;
				case "Savings Payment":
					mHappinessRates[6] += mPersonalityCoef[6];
					break;
				case "Entertainment Payment":
					mHappinessRates[7] += mPersonalityCoef[7];
					break;	
				}
			}
		}
		
		for (double r : mHappinessRates ) {
			if ( mHappiness > 0 && mHappiness <= 100 )
				mHappiness += r;
		}
		
		mHouseHold.update(this);
	}
	
	public ArrayList<String> getThingsThatMakeMeHappy() {
		
		ArrayList<String> values = new ArrayList<String>();
		
		for ( int i = 0; i < mPersonalityCoef.length; i++ ) {
			if (mPersonalityCoef[i] > 0 ) {
				switch(i) {
				case 0:
					values.add("Rent");
					break;
				case 1:
					values.add("Utility");
					break;
				case 2:
					values.add("Groceries");
					break;
				case 3:
					values.add("Taxes");
					break;
				case 4:
					values.add("Car");
					break;
				case 5:
					values.add("Insurance");
					break;
				case 6:
					values.add("Savings");
					break;
				case 7:
					values.add("Entertainment");
					break;
				}
			}
		}
		
		return values;
	}

	public double getHappiness() {
		return mHappiness;
	}

	public String getName() {
		return mName;
	}
	
}
