package com.benjica.csci136.lb5.simu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.Timer;

/**
 * TODO Implement Class Definition for LifeEngine.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class LifeEngine extends Timer implements Serializable, ActionListener {
	

	private int mDate = 0;
	
	private Household mHousehold = new Household();
	
	public LifeEngine() {
		super(1000, null);
		addActionListener(this);
		
		Adult you = new Adult("Tom Foolery", this);
		Child them = new Child("Fat Albert", this);
		
		mHousehold.add(you);
		mHousehold.add(them);
	}

	public int getDate() {
		return mDate;
	}

	public Household getHousehold() {
		return mHousehold;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		mDate += 1;
		
	}
	
}
