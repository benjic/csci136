package com.benjica.csci136.lb5.simu;

/**
 * TODO Implement Class Definition for Child.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Child extends Person {

	public Child(String name, LifeEngine le) {
		super(name, le);
	}
	
}
