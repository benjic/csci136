package com.benjica.csci136.lb5;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * TODO Implement Class Definition for HappinessPersonComponent.java
 * Apr 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class HappinessPersonComponent extends JPanel implements
		ListCellRenderer {

	

	public HappinessPersonComponent() {
		
	}
	
	@Override
	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		return this;
	}
	
}
