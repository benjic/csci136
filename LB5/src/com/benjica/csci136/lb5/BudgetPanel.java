package com.benjica.csci136.lb5;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.benjica.csci136.lb5.simu.Ledger;
import com.benjica.csci136.lb5.simu.LifeEngine;
import com.benjica.csci136.lb5.simu.LineItem;

/**
 * TODO Implement Class Definition for BudgetPanel.java
 * Mar 19, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class BudgetPanel extends JPanel implements ActionListener {
	
	private JLabel mCurrentBalanceLabel = new JLabel("Current Balance");
	private JLabel mCurrentBalanceValueLabel = new JLabel();
	
	private JLabel mRentLabel = new JLabel("Rent");
	private JLabel mUtilityLabel = new JLabel("Utilities");
	private JLabel mGroceriesLabel = new JLabel("Groceries");
	private JLabel mTaxesLabel = new JLabel("Taxes");
	private JLabel mCarLabel = new JLabel("Car");
	private JLabel mInsuranceLabel = new JLabel("Insurance");
	private JLabel mSavingsLabel = new JLabel("Savings");
	private JLabel mEntertainmentLabel = new JLabel("Entertainment");
	
	private JTextField mRentTextField = new JTextField(20);
	private JTextField mUtilityTextField = new JTextField(20);
	private JTextField mGroceriesTextField = new JTextField(20);
	private JTextField mTaxesTextField = new JTextField(20);
	private JTextField mCarTextField = new JTextField(20);
	private JTextField mInsuranceTextField = new JTextField(20);
	private JTextField mSavingsTextField = new JTextField(20);
	private JTextField mEntertainmentTextField = new JTextField(20);
	
	private JButton mRentButton = new JButton("Add");
	private JButton mUtilityButton = new JButton("Add");
	private JButton mGroceriesButton = new JButton("Add");
	private JButton mTaxesButton = new JButton("Add");
	private JButton mCarButton = new JButton("Add");
	private JButton mInsuranceButton = new JButton("Add");
	private JButton mSavingsButton = new JButton("Add");
	private JButton mEntertainmentButton = new JButton("Add");
	
	private JTable mLedgerTable;
	
	 GroupLayout mLayout;
	
	
	LifeEngine mLifeEngine;
	Ledger mLedger;
	
	NumberFormat cf;
	
	public BudgetPanel(LifeEngine le) {
		
		// Utility Classes
		cf = NumberFormat.getCurrencyInstance();
		
		mLifeEngine = le;
		//mLifeEngine.addActionListener(this);
		mLedger = mLifeEngine.getHousehold().getLedger();
		
		mLedgerTable = new JTable(mLedger);
		JScrollPane ledgerScroll = new JScrollPane(mLedgerTable);
		
		mLayout = new GroupLayout(this);
		setLayout(mLayout);
		
		mLayout.setAutoCreateContainerGaps(true);
		mLayout.setAutoCreateGaps(true);
		
		mCurrentBalanceValueLabel.setText(cf.format(10000));
		
		mRentTextField.setText(cf.format(1000));
		mUtilityTextField.setText(cf.format(400));
		mGroceriesTextField.setText(cf.format(500));
		mTaxesTextField.setText(cf.format(3000));
		mCarTextField.setText(cf.format(500));
		mInsuranceTextField.setText(cf.format(500));
		mSavingsTextField.setText(cf.format(2000));
		mEntertainmentTextField.setText(cf.format(0));
		
		mRentButton.addActionListener(this);
		mUtilityButton.addActionListener(this);
		mGroceriesButton.addActionListener(this);
		mTaxesButton.addActionListener(this);
		mCarButton.addActionListener(this);
		mInsuranceButton.addActionListener(this);
		mSavingsButton.addActionListener(this);
		mEntertainmentButton.addActionListener(this);
		
		mLayout.setHorizontalGroup(
				mLayout.createParallelGroup()
					.addGroup(
							mLayout.createSequentialGroup()
						.addGroup(
								mLayout.createParallelGroup()
									.addComponent(mRentLabel)
									.addComponent(mUtilityLabel)
									.addComponent(mGroceriesLabel)
									.addComponent(mTaxesLabel)
									.addComponent(mCarLabel)
									.addComponent(mInsuranceLabel)
									.addComponent(mSavingsLabel)
									.addComponent(mEntertainmentLabel))
						.addGroup(
								mLayout.createParallelGroup()
									.addComponent(mRentTextField)
									.addComponent(mUtilityTextField)
									.addComponent(mGroceriesTextField)
									.addComponent(mTaxesTextField)
									.addComponent(mCarTextField)
									.addComponent(mInsuranceTextField)
									.addComponent(mSavingsTextField)
									.addComponent(mEntertainmentTextField))
						.addGroup(
								mLayout.createParallelGroup()
									.addComponent(mRentButton)
									.addComponent(mUtilityButton)
									.addComponent(mGroceriesButton)
									.addComponent(mTaxesButton)
									.addComponent(mCarButton)
									.addComponent(mInsuranceButton)
									.addComponent(mSavingsButton)
									.addComponent(mEntertainmentButton)))
					.addComponent(ledgerScroll));
		
		mLayout.setVerticalGroup(
				mLayout.createSequentialGroup()
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mRentLabel)
								.addComponent(mRentTextField)
								.addComponent(mRentButton))
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mUtilityLabel)
								.addComponent(mUtilityTextField)
								.addComponent(mUtilityButton))
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mGroceriesLabel)
								.addComponent(mGroceriesTextField)
								.addComponent(mGroceriesButton))
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mTaxesLabel)
								.addComponent(mTaxesTextField)
								.addComponent(mTaxesButton))
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mCarLabel)
								.addComponent(mCarTextField)
								.addComponent(mCarButton))
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mInsuranceLabel)
								.addComponent(mInsuranceTextField)
								.addComponent(mInsuranceButton))
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mSavingsLabel)
								.addComponent(mSavingsTextField)
								.addComponent(mSavingsButton))
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mEntertainmentLabel)
								.addComponent(mEntertainmentTextField)
								.addComponent(mEntertainmentButton))
					.addComponent(ledgerScroll));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		try {
			
			if ( e.getSource() == mRentButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Rent Payment",
						deductAmount(mRentTextField)));
			
			if ( e.getSource() == mUtilityButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Utility Payment",
						deductAmount(mUtilityTextField)));
			
			if ( e.getSource() == mGroceriesButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Groceries Payment",
						deductAmount(mGroceriesTextField)));
			
			if ( e.getSource() == mTaxesButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Taxes Payment",
						deductAmount(mTaxesTextField)));
			
			if ( e.getSource() == mCarButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Car Payment",
						deductAmount(mCarTextField)));
			
			if ( e.getSource() == mInsuranceButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Insurance Payment",
						deductAmount(mInsuranceTextField)));
			
			// TODO: Savings should be an account that can deposit and withdraw
			if ( e.getSource() == mSavingsButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Savings Deposit",
						deductAmount(mSavingsTextField)));
			
			if ( e.getSource() == mEntertainmentButton )
				mLedger.add(new LineItem(
						mLifeEngine.getDate(),
						"Entertainment Payment",
						deductAmount(mEntertainmentTextField)));
			
			} catch (ParseException e1) {
				// TODO: Create alert dialog saying the user input was not parseable
				e1.printStackTrace();
			}
		
		
	}
	
	private double deductAmount(JTextField input) throws ParseException {
		try {
			
			double v = -1 * Math.abs(Double.parseDouble(input.getText()));
			input.setText(cf.format(v));
			
			return v;
		} catch (NumberFormatException e) {
			return -1 * Math.abs(cf.parse(input.getText()).doubleValue());
		}
	}
}
