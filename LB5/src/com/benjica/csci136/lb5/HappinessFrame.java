package com.benjica.csci136.lb5;

import javax.swing.JFrame;

import com.benjica.csci136.lb5.simu.LifeEngine;

/**
 * TODO Implement Class Definition for HappinessFrame.java
 * Apr 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class HappinessFrame extends JFrame {
	public HappinessFrame(LifeEngine le) {
		add(new HappinessPanel(le));
	}
}
