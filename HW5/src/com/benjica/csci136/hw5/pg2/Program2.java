package com.benjica.csci136.hw5.pg2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.benjica.csci136.hw5.Sort;


/**
 * TODO Implement Class Definition for Program2.java
 * Apr 27, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 * @param <T>
 */
public class Program2 {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		ArrayList<Student> students = new ArrayList<Student>();
		Scanner fileScanner = new Scanner(new File("data/pg2/students1.csv"));
		
		while ( fileScanner.hasNextLine() ) {
			
			String[] params = fileScanner.nextLine().split(",");
			
			try {
				students.add(new Student(params[0], 
						Integer.parseInt(params[1]),
						Integer.parseInt(params[2])));
			} catch (NumberFormatException ex) {
				System.err.printf("There was an error processing student data.");
				System.exit(1);
			}
		}
				
		fileScanner.close();
		
		Scanner userScanner = new Scanner(System.in);
		
		boolean user_input = true;
		char sort = 'a';
		
		do {
			
			System.out.printf("Would you like to sort the student list in [A]scending or [D]esencing order?\n");
			
			try {
				sort = userScanner.next("[AaDd]").toLowerCase().charAt(0);
				user_input = false;
			} catch (InputMismatchException ex) {
				System.out.printf("I did not understand your input!");
				userScanner.next();
			}
			
		} while(user_input);
		
		switch (sort) {
		case 'a':
			Sort.InsertionSort(students);
			break;
		case 'd':
			Sort.ReverseInsertionSort(students);
			break;
		}
		
		userScanner.nextLine();
		
		for (Student s : students ) {
			System.out.println(s);
		}
		
		user_input = true;
		String name = "";
		do {
			
			System.out.printf("Please enter the name of student to search\n");
			
			try {
				name = userScanner.nextLine();
				user_input = false;
			} catch (InputMismatchException ex) {
				System.out.printf("I did not understand your input!");
				userScanner.next();
			}
			
		} while(user_input);
		
		try {
			Student myStudent = Student.findStudentByName(students, name);
			
			System.out.println(myStudent);
		} catch (NameNotFoundException e) {
			System.err.printf("%s did not match any students in the list.\n", name);
			System.exit(0);
		}
		userScanner.close();
	}
}
