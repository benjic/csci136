package com.benjica.csci136.hw5.pg2;

import java.text.NumberFormat;
import java.util.Comparator;
import java.util.List;

import com.benjica.csci136.hw5.Sort;

/**
 * TODO Implement Class Definition for Student.java
 * Apr 27, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Student implements Comparable<Student>, Comparator<Student> {
	
	
	
	private static final NumberFormat nf = NumberFormat.getCurrencyInstance();
	
	private String mName;
	private int mIncome;
	private int mAptitude;
	
	public Student(String name, int income, int aptitude) {
		mName = name;
		mIncome = income;
		mAptitude = aptitude;
	}

	public String getName() {
		return mName;
	}

	@Override
	public int compareTo(Student o) {
		if (mIncome != o.mIncome ) {
			if ( mAptitude != o.mAptitude) {
				return ((int) Math.signum(mAptitude - o.mAptitude));
			}
			return ((int) Math.signum(mIncome - o.mIncome)); 
		}
		return 0;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %d", mName, nf.format(mIncome), mAptitude);
	}

	@Override
	public int compare(Student o1, Student o2) {
		return o1.mName.compareTo(o2.mName);
	}
	
	public static Student findStudentByName(List<Student> students, String name) throws NameNotFoundException {
		
		Sort.InsertionSort(students, new Comparator<Student>() {

			@Override
			public int compare(Student a, Student b) {
				return a.mName.compareTo(b.mName);
			}
			
		});
		
		if ( students.size() == 0 )
			throw new NameNotFoundException();
		

			if ( name.compareTo( students.get(students.size() / 2).getName()) < 0 ) {
				return findStudentByName(students.subList(0, (students.size()-1)/ 2), name);
			} else if ( name.compareTo( students.get(students.size() / 2).getName()) > 0 ) {
				return findStudentByName(students.subList((students.size()-1)/ 2, students.size()-1), name);
			} else {
				return students.get(students.size() / 2);
			}
		
	}
	
	
}
