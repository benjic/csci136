package com.benjica.csci136.hw5.pg2;

import java.util.Comparator;

public class NameComparator implements Comparator<Student> {

	@Override
	public int compare(Student a, Student b) {
		return a.getName().compareTo(b.getName());
	}
	/**
	 * TODO Implement Class Definition for NameComparator.java
	 * Apr 28, 2014
	 *
	 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
	 */
}
