package com.benjica.csci136.hw5.pg3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * TODO Implement Class Definition for Program3.java
 * Apr 30, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program3 {
	
	public static void main(String[] args ) throws FileNotFoundException {
		
		if ( args.length != 2 ) {
			System.err.println("Invaild number of arguments: Program3 <file1> <file2>");
			System.exit(1);
		}
		try {
			Scanner file1 = new Scanner(new File(args[0]));
			Scanner file2 = new Scanner(new File(args[1]));
		
			double similiarity = scanDocuments(file1, file2, 0, 0);
			
			if ( similiarity > 0.05 ) {
				System.out.printf("!!PLAGAIRISM LIKELY!! The files were %1.2f%% simliar!!\n", similiarity * 100);
			} else {
				System.out.printf("Plagairism no likely, %1.2f%% simliar.\n", similiarity * 100);
			}
			
		} catch (FileNotFoundException ex) {
			System.err.printf("The given file was not found.\n");
			System.exit(1);
		}
		
	}
	
	public static double scanDocuments(Scanner documentA, Scanner documentB, double count, double words)  {

		if ( !(documentA.hasNext() && documentB.hasNext()) )
			return count / words;

		String[] a = documentA.nextLine().split(" ");
		String[] b = documentB.nextLine().split(" ");
		
		for( int i = 0; i < Math.min(a.length, b.length); i++) {
			if (a[i].equals(b[i])) {
				count++;
			}
		}

		return scanDocuments(documentA, documentB, count, words + Math.min(a.length, b.length));
	}
}
