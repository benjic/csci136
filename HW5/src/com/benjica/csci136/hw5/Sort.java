package com.benjica.csci136.hw5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * TODO Implement Class Definition for Sort.java
 * Apr 27, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Sort {
	
	
	public static <T extends Comparable> void InsertionSort(List<T> list) {
		InsertionSort(list, 1, false);		
	}
	
	public static <T> void InsertionSort(List<T> list, Comparator<T> comp) {
		for ( int i = 1; i < list.size(); i++) 
			for( int j = i; j > 1; j -= 1 )
					if ( comp.compare(list.get(j), list.get(j-1)) < 0 ) 
						swap(list, j, j-1);
				
	}
	
	public static <T extends Comparable> void ReverseInsertionSort(List<T> list) {
		InsertionSort(list, 1, true);
	}
	
	public static <T extends Comparable> void SelectionSort(List<T> list) {
		SelectionSort(list, false);
	}
	
	public static <T extends Comparable> void ReverseSelectionSort(List<T> list) {
		SelectionSort(list, true);
	}
	
	public static <T extends Comparable> void BubbleSort(List<T> list) {
		BubbleSort(list, false);
	}
	
	public static <T extends Comparable> void ReverseBubbleSort(List<T> list) {
		BubbleSort(list, true);
	}
	
	private static  <T extends Comparable> void InsertionSort(List<T> list, int skip, boolean reverse) {
		
		for ( int i = 2; i < list.size(); i++) 
			for( int j = i; j > 1; j -= skip )
				if ( reverse) {
					if ( list.get(j).compareTo(list.get(j-skip)) < 0 ) 
						swap(list, j, j-skip);
				} else {
					if ( list.get(j-skip).compareTo(list.get(j)) < 0 )
						swap(list, j, j-skip);
				}
	}
	
	
	private static <T extends Comparable> void SelectionSort(List<T> list, boolean reverse) {
		for ( int i = 0; i < list.size(); i++) {
			int k = i;
			
			for ( int j = i+1; j < list.size(); j++)
				if ( reverse ) {	
					if ( list.get(j).compareTo(list.get(i)) < 0 )
						k = j; 
				} else {
					if ( list.get(i).compareTo(list.get(j)) < 0 )
						k = j;
				}
					
			swap(list, i, k );
					
		}
			
	}
	
	private static <T extends Comparable> void BubbleSort(List<T> list, boolean reverse) {
		for (int i = 0; i < list.size(); i++ ) {
			boolean swapped = false;
			
			for (int j = list.size()-1; j > i+1; j--)
				if ( reverse ) {
					if (list.get(j-1).compareTo(list.get(j)) < 0) {
						swap(list, j-1, j);
						swapped = true;
					}
				} else {
					if (list.get(j).compareTo(list.get(j-1)) < 0) {
						swap(list, j, j-1);
						swapped = true;
					}
				}
			
			if ( !swapped )
				break;
			
		}
	}
	
	private static <T> void swap(List<T> l, int j, int k) {
		T a = l.get(j);
		T b = l.get(k);
		
		l.set(j, b);
		l.set(k, a);
	}
}
