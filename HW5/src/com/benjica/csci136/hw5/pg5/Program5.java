package com.benjica.csci136.hw5.pg5;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * TODO Implement Class Definition for Program5.java
 * May 5, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program5 extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;

	private Timer mTimer;

	private JButton mSwingButton;
	private JButton mPitchButton;
	
	private GroupLayout mLayout;
	private BaseballJPanel mBaseballJPanel;
	
	public Program5() {
		
		
		mBaseballJPanel = new BaseballJPanel();
		mSwingButton = new JButton("Swing");
		
		mSwingButton.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mBaseballJPanel.swing();
			}
			
		});
		
		mPitchButton = new JButton("Pitch");
		
		mPitchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				mBaseballJPanel.pitch();
			}
			
		});
		mTimer = new Timer(10, this);
		mTimer.start();
		
		mLayout = new GroupLayout(this);
		setLayout(mLayout);
		
		mLayout.setAutoCreateContainerGaps(true);
		mLayout.setAutoCreateGaps(true);
		
		mLayout.setVerticalGroup(
				mLayout.createSequentialGroup()
					.addComponent(mBaseballJPanel)
					.addGroup(
							mLayout.createParallelGroup()
								.addComponent(mSwingButton)
								.addComponent(mPitchButton)));
		
		mLayout.setHorizontalGroup(
				mLayout.createParallelGroup(Alignment.CENTER)
					.addComponent(mBaseballJPanel)
					.addGroup(
							mLayout.createSequentialGroup()
								.addComponent(mSwingButton)
								.addComponent(mPitchButton)));
		
	}
	
	public static void main(String[] args) {
		JFrame c = new JFrame();
		
		c.setTitle("Baseball");
		c.add(new Program5());
		
		c.pack();
		c.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		repaint();
	}
}
