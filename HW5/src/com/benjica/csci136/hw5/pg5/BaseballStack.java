package com.benjica.csci136.hw5.pg5;

import java.util.LinkedList;
import java.util.Random;

/**
 * TODO Implement Class Definition for BaseballStack.java
 * May 5, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class BaseballStack{
	
	private LinkedList<Baseball> mList = new LinkedList<Baseball>();
	
	public BaseballStack(int numberOfBalls) {
		
		Random rand = new Random();
		
		for (int i = 0; i < numberOfBalls; i++) {
			push(new Baseball(rand.nextInt(4) + 1));
		}
	}
	
	public boolean empty() {
		return mList.isEmpty();
	}
	
	public Baseball peek() {
		return mList.get(0);
	}
	
	public Baseball pop() {
		return mList.remove(0);
	}
	
	public Baseball push(Baseball b) {
		mList.add(0, b);
		
		return b;
	}
}
