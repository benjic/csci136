package com.benjica.csci136.hw5.pg5;

public class Baseball {

	private int mSpeed;
	
	public Baseball(int speed) {
		mSpeed = speed;
	}

	public int getSpeed() {
		return mSpeed;
	}
	
}
