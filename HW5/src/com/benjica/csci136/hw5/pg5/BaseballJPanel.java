package com.benjica.csci136.hw5.pg5;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * TODO Implement Class Definition for BaseballJPanel.java
 * May 5, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class BaseballJPanel extends JPanel {
	
	private int mCount;
	private String mStatus;
	
	private Random mRand;
	
	private Image mBackground;
	private Image mBall;

	private boolean mIsPitched = false;
	private boolean mSwing = false;
	
	private BaseballStack mBaseBallStack;
	private Baseball mBaseball;
	
	public BaseballJPanel() {
		
		mRand = new Random();
		
		mBaseBallStack = new BaseballStack(10);
		
		try {
			mBackground = ImageIO.read(new File("data/pg5/background.jpg"));
			mBall = ImageIO.read(new File("data/pg5/ball.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		setPreferredSize(new Dimension(761, 450));
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
		g.drawImage(mBackground, 
				0, 0, 
				mBackground.getWidth(null), mBackground.getHeight(null),
				0, 0, 
				mBackground.getWidth(null), mBackground.getHeight(null),
				null);
		
		
		if ( mCount < 250 ) {
			if ( mIsPitched  ) {
				g.drawImage(mBall, 
						getWidth()/2 - mCount, getHeight()/2 - mCount - 50,
						getWidth()/2 + mCount, getHeight()/2 + mCount - 50 ,
						0, 0,
						mBall.getWidth(null), mBall.getHeight(null),
						null);
				
				mCount += mBaseball.getSpeed();
			}
		} else {
			mIsPitched = false;
			g.setColor(Color.RED);
			g.setFont(new Font("SERIF", Font.PLAIN, 48));
			g.drawString(mStatus, 100, 200);
		}
		
		
	}
	
	public void swing() {
		mSwing = true;
		if ( mIsPitched ) {
			if ( mRand.nextBoolean() ) {
				mStatus = String.format("Wow! A %d yd home run!", mRand.nextInt(301) + 1);
			} else {
				mStatus = "You missed!";
			}
		}
	}
	
	public void pitch() {
		
		if ( !mBaseBallStack.empty() ) {
			mSwing = false;
			mCount = 0;
			mIsPitched = true;
			
			mBaseball = mBaseBallStack.pop();
			
			if ( mRand.nextBoolean() ) {
				mStatus = "The pitch was a strike";
			} else {
				mStatus = "The pitch was a ball";
			}
		} else {
			mStatus = "You've run out of pitches";
		}
	}
}
