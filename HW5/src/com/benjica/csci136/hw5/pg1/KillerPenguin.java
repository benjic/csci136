package com.benjica.csci136.hw5.pg1;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import com.benjica.csci136.hw5.pg1.ui.DesertZombieJPanel;

public class KillerPenguin extends DesertCharacter implements KeyListener {
	
	public KillerPenguin() throws IOException {
		super("data/pg1/tux.png");
	}
	
	@Override
	public void keyPressed(KeyEvent k) {
		switch ( k.getKeyCode() ) {
		case KeyEvent.VK_LEFT:
			moveForward();
			break;
		case KeyEvent.VK_RIGHT:
			moveBackward();
			break;
		}
		
		DesertZombieJPanel source = (DesertZombieJPanel) k.getSource();
		
		for (DesertCharacter c : source.getCharacters()) {
			if (isCollided(c) && !c.isDead())
				c.takeHit();
		}
		
		source.validate();
		source.repaint();
	}

	@Override
	public void keyReleased(KeyEvent k) {
		// Noop
	}

	@Override
	public void keyTyped(KeyEvent k) {
		// Noop
	}
	
	@Override
	public void moveForward() {
		mX = mX > 10 ? mX - 10 : 0;
	}
	
	@Override
	public void moveBackward() {
		mX = mX < 750 ? mX + 10 : 750;
	}
	
	@Override
	public void takeHit() {
		mHP -= 25;
		mX -= 100;
	}
}
