package com.benjica.csci136.hw5.pg1.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

import com.benjica.csci136.hw5.pg1.DesertCharacter;
import com.benjica.csci136.hw5.pg1.Fiend;
import com.benjica.csci136.hw5.pg1.KillerPenguin;

/**
 * TODO Implement Class Definition for DesertZombieJPanel.java
 * Apr 26, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class DesertZombieJPanel extends JPanel implements ActionListener  {
	
	private static Random mRandom = new Random();
	
	private Image mBackground;
	private KillerPenguin mCharacter;
	
	private Timer mTimer;
	private ArrayList<DesertCharacter> mCharacters;

	public DesertZombieJPanel() throws IOException {
		
		mBackground = ImageIO.read(new File("data/pg1/desert_BG.png"));
		
		mCharacters = new ArrayList<DesertCharacter>();
		
		mCharacter = new KillerPenguin();
		addKeyListener(mCharacter);		
		
		mTimer = new Timer(1000, this);
		mTimer.start();
		
		setFocusable(true);
		requestFocus();
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
		g.drawImage(mBackground, 0, 0, null);
		
		for ( DesertCharacter c : mCharacters ) {
			c.drawCharacter(g);
		}
		
		mCharacter.drawCharacter(g);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
				
		try {
			if ( mCharacters.size() < 5 ) 
				if ( mRandom.nextInt() % 5 == 0 ) {
					
					Fiend f = new Fiend();
					mTimer.addActionListener(f);
					
					mCharacters.add(new Fiend());
				}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		for ( DesertCharacter c : mCharacters ) {
			if ( !c.isDead() )
				c.moveForward();
			
			if ( c.isCollided(mCharacter) && !c.isDead())
				mCharacter.takeHit();
		}
		
		validate();
		repaint();
	}
	
	public ArrayList<DesertCharacter> getCharacters() {
		return mCharacters;
	}
	
}
