package com.benjica.csci136.hw5.pg1;

import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;

import com.benjica.csci136.hw5.pg1.ui.DesertZombieJPanel;

/**
 * TODO Implement Class Definition for Program1.java
 * Apr 26, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program1 extends JFrame {
	
	public Program1() throws IOException {
		setTitle("Desert Zombie");
		DesertZombieJPanel zPanel = new DesertZombieJPanel();
		
		setPreferredSize(new Dimension(800,400));
		this.setResizable(false);
		
		add(zPanel);
	}
	
	public static void main(String[] args) throws IOException {
		
		Program1 pg1 = new Program1();
		
		pg1.pack();
		pg1.setVisible(true);
		
	}
}
