package com.benjica.csci136.hw5.pg1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

/**
 * TODO Implement Class Definition for Character.java
 * Apr 26, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public abstract class DesertCharacter {
	
	private Random mRandom = new Random();
	
	protected Image mCharacterMap;
	
	protected int mX = 0;
	protected int mY = 290;
	protected int mHP = 100;
	
	public DesertCharacter(String path) throws IOException {
		mCharacterMap = ImageIO.read(new File(path));
	}
	
	public void drawCharacter(Graphics g) {
				
		g.drawImage(mCharacterMap, mX, mY, null);
				
		if ( mHP <= 100)
			g.setColor(Color.GREEN);
		if ( mHP <= 50)
			g.setColor(Color.YELLOW);
		if ( mHP <= 25)
			g.setColor(Color.RED);
		
		g.fillRect(mX, mY-10, ((int) (mCharacterMap.getWidth(null) * (mHP / 100.0))), 5);
	}
	
	public void moveForward() {
		mX = mX > 10 ? mX - mRandom.nextInt(50) : 0;
	}
	
	public void moveBackward() {
		mX = mX < 750 ? mX + mRandom.nextInt(50) : 750;
	}
	
	public void takeHit() {
		mHP -= 25;
		mX += 100;
	}
	
	public boolean isCollided(DesertCharacter c) {
		return (mCharacterMap.getWidth(null) + mX) > c.mX
				&& (mCharacterMap.getWidth(null) + mX < c.mX + c.mCharacterMap.getWidth(null)); 
	}
	
	public boolean isDead() {
		
		try {
			if ( mHP <= 0 ) {
				mCharacterMap = ImageIO.read(new File("data/pg1/ghoul_dead.png"));
				return true;
			} else {
				return false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return mHP <= 0;
	}
}
