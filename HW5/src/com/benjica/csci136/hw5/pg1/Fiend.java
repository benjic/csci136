package com.benjica.csci136.hw5.pg1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * TODO Implement Class Definition for Fiend.java
 * Apr 26, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Fiend extends DesertCharacter implements ActionListener {

	public Fiend() throws IOException {
		super("data/pg1/ghoul.png");
		
		mX = 700;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		moveForward();
	}
}
