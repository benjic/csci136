package com.benjica.csci136.hw5.pg4;

import java.util.LinkedList;
import java.util.Queue;

/**
 * TODO Implement Class Definition for TicketPriorityQueue.java
 * May 5, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class TicketPriorityQueue extends LinkedList<Ticket> implements Queue<Ticket> {

	private static final long serialVersionUID = 1L;

	@Override
	public boolean add(Ticket e) {
		for ( int i = 0; i < size(); i++ ) {
			if ( e.compareTo(get(i)) == -1 ) {
				add(i, e);
				return true;
			}
		}
		
		return super.add(e);
	}
	
}
