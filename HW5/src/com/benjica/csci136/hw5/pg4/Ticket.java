package com.benjica.csci136.hw5.pg4;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * TODO Implement Class Definition for Ticket.java
 * May 5, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Ticket implements Comparable<Ticket> {
	private String mName;
	private DateTime mDate;
	
	public Ticket(String name, String date, String time) {
		mName = name;
		
		DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yyyy hh:mm a");
		mDate = fmt.parseDateTime(String.format("%s %s", date, time));
	}
	
	@Override
	public String toString() {
		return String.format("%s -- %s", mName, mDate.toString());
	}

	@Override
	public int compareTo(Ticket arg0) {
		return mDate.compareTo(arg0.mDate);
	}
}
