package com.benjica.csci136.hw5.pg4;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * TODO Implement Class Definition for Program4.java
 * May 5, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program4 {
	
	public static void main(String[] args) throws FileNotFoundException {
		Scanner file = new Scanner(new File("data/pg4/Customers.csv"));
		
		TicketPriorityQueue tpq = new TicketPriorityQueue();
		
		while ( file.hasNextLine() ) {
			String[] params = file.nextLine().split(",");
			
		tpq.add( new Ticket(params[0], params[1], params[2]) );
		}
		
		System.out.println("Here is the queue of movie tickets:\n");
		while ( !tpq.isEmpty() ) {
			System.out.println(tpq.remove());
		}
		
		file.close();
	}
}
