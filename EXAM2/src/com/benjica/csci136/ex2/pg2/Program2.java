package com.benjica.csci136.ex2.pg2;

import java.io.FileNotFoundException;
import java.util.Collections;

import com.benjica.csci136.ex2.pg2.utility.Plant;
import com.benjica.csci136.ex2.pg2.utility.PlantArrayList;
import com.benjica.csci136.ex2.pg2.utility.Sorting;

/**
 * A driver class for sorting plant data.
 * Apr 18, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program2 {

	public static void main(String[] args) throws FileNotFoundException {
		
		// Create a new list of plants 
		// Please note that I am using a truncated version of the data
		// It seems as though the ASCII encoding used for the data is malformed
		// for my machine resulting in line breaks in the middle of lines. This
		// causes the parser to fail because of an improper number of fields
		// 
		// PlantArrayList plants = new PlantArrayList("data/Plants.csv");
		PlantArrayList plants = new PlantArrayList("data/Plants_truncated.csv");
		
		// I am not sure where the selection or insertion sort are so I am going
		// to use the standard library merge sort.
		//Collections.sort(plants);
		// 
		// Nevermind, I'll just implement a InsertionSort
		
		Sorting.InsertionSort(plants);
		
		// I also implemented others if you want to try.
		//Sorting.SelectionSort(plants);
		//Sorting.BubbleSort(plants);
		
		// Iterate over the plants after sorting
		for(Plant p : plants) {
			System.out.println(p);
		}
	}
	
}
