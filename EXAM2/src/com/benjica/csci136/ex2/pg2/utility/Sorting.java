package com.benjica.csci136.ex2.pg2.utility;


/**
 * A collection of sorting algorithms.
 * Apr 18, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Sorting {
	
	public static void InsertionSort(PlantArrayList plants) {
		InsertionSort(plants, 1);		
	}
	
	private static void InsertionSort(PlantArrayList plants, int skip) {
		
		for ( int i = 2; i < plants.size(); i++) 
			for( int j = i; j > 1; j -= skip ) 
				if ( plants.get(j).compareTo(plants.get(j-skip)) < 0 ) 
					swap(plants, j, j-skip);	
	}
	
	public static void SelectionSort(PlantArrayList plants) {
		for ( int i = 0; i < plants.size(); i++) {
			int k = i;
			
			for ( int j = i+1; j < plants.size(); j++)
				if ( plants.get(j).compareTo(plants.get(i)) < 0 )
					k = j;
					
			swap(plants, i, k );
					
		}
			
	}
	
	public static void BubbleSort(PlantArrayList plants) {
		for (int i = 0; i < plants.size(); i++ ) {
			boolean swapped = false;
			
			for (int j = plants.size()-1; j > i+1; j--)
				if (plants.get(j).compareTo(plants.get(j-1)) < 0) {
					swap(plants, j, j-1);
					swapped = true;
				}
			
			if ( !swapped )
				break;
			
		}
	}
	

	
	private static void swap(PlantArrayList l, int j, int k) {
		Plant a = l.get(j);
		Plant b = l.get(k);
		
		l.set(j, b);
		l.set(k, a);
	}
}
