package com.benjica.csci136.ex2.pg2.utility;

/**
 * Plant is a data structure to facilitate the plant data and implement
 * the sorting properties using the comparator interface.
 * 
 * Apr 18, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Plant implements Comparable<Plant>{
	

	// Class fields
	private int mAmount;
	private int mID;
	private int mYear;

	private String mCounty;
	private String mGenus;
	private String mSpecies;
	private String mState;
	

	/** 
	 * Primary Constructor
	 * @param id PlantID
	 * @param genus Plant Genus
	 * @param species Plant Species
	 * @param year Year of collection
	 * @param state State of collection
	 * @param county County of Collection
	 * @param amount Amount collected
	 */
	public Plant(int id, String genus, String species, int year,
			String state, String county, int amount) {
		
		mID 		= id;
		mGenus 		= genus;
		mSpecies	= species;
		mYear 		= year;
		mState 		= state;
		mCounty 	= county;
		mAmount 	= amount;
	}

	@Override
	public int compareTo(Plant b) {
		
		// This comparator interface sorts plants based on the specifications
		// in EXAM2 for CSCI136. Primary sort is by state alphabetically followed
		// by county alphabetically. This is followed by Year descending and
		// amount descending
		
		if ( mState.compareTo(b.getState()) == 0 ) {
			if ( mCounty.compareTo(b.getCounty()) == 0 ){
				if ( mYear - b.getYear() == 0 ) {
					// Sort by amount in descending order
					return  Integer.signum(b.getAmount() - mAmount );
				} else {
					// Sort by year in descending order
					return  Integer.signum(b.getYear() - mYear);
				}
			} else {
				return mCounty.compareTo(b.getCounty()); // Sort by county ascending
			}
			
		} else {
			return mState.compareTo(b.getState()); // Sort By state ascending
		}
		
	}

	@Override
	public String toString() {
		// Lets build csv's
		return String.format("%d,%s,%s,%d,%s,%s,%d", 
				mID,
				mGenus,
				mSpecies,
				mYear,
				mState,
				mCounty,
				mAmount);
	}

	public int getAmount() {
		return mAmount;
	}


	public int getID() {
		return mID;
	}


	public int getYear() {
		return mYear;
	}


	public String getCounty() {
		return mCounty;
	}


	public String getGenus() {
		return mGenus;
	}


	public String getSpecies() {
		return mSpecies;
	}


	public String getState() {
		return mState;
	}
	
}
