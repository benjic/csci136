package com.benjica.csci136.ex2.pg2.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A simple abstraction to allow rapid ingestation of data from the given
 * file. 
 * Apr 18, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class PlantArrayList extends ArrayList<Plant> {
	
	private static final long serialVersionUID = 1L;

	/**
	 * The constructor takes a path as an argument to a list of well
	 * formed Plant data.
	 * 
	 * @param filePath
	 * @throws FileNotFoundException
	 */
	public PlantArrayList(String filePath) throws FileNotFoundException {
		super();
		
		File f = new File(filePath);
		
		Scanner fileScanner = new Scanner(f);
		fileScanner.nextLine();
			
		// Iterate over every line in file
		while (fileScanner.hasNextLine()) {
		
			// Blow up line
			String[] params = fileScanner.nextLine().split(",");
			
				// Parse data
				add( new Plant(
						Integer.parseInt(params[0]), 	// ID
						params[1],						// Genus
						params[2],						// Species
						Integer.parseInt(params[3]),	// Year Collected
						params[4],						// State
						params[5],						// County
						Integer.parseInt(params[6])));	// Amount
				
			
		} 
		
		fileScanner.close();			
	}
	
}
