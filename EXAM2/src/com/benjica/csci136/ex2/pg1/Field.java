package com.benjica.csci136.ex2.pg1;

/**
 * A relization of an Event representing field events.
 * Apr 16, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Field extends Event {

	public Field(int id, String name, double record, String units) {
		super(id, name, record, units);
	}

	@Override
	public String description() {
		return String.format("%s is a field event with a record of %s %s.", 
				mName,
				mRecord,
				mUnits);
	}

	@Override
	public void updateRecord(double record) {
		mRecord = record;
	}
}
