package com.benjica.csci136.ex2.pg1;

import java.io.FileNotFoundException;
import java.util.Random;

import com.benjica.csci136.ex2.pg1.data.EventArrayList;
import com.benjica.csci136.ex2.pg1.data.Racer;
import com.benjica.csci136.ex2.pg1.data.RacerArrayList;

/**
 * A driver class for Program where events are ran.
 * Apr 16, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program1 {

	public static void main(String[] args) throws FileNotFoundException {
	
		// Utility Classes
		Random rand = new Random();
		
		// Build lists
		EventArrayList events = new EventArrayList("data/Events.csv");
		RacerArrayList racers = new RacerArrayList("data/Racers.csv");

		// Randomly choose ten racers
		for ( int i = 0; i < 10; i++ ) {
			Racer r = racers.get(rand.nextInt(events.size()));
			
			// Iterate over events to find their event
			for(Event e : events) {
				if ( r.getEventID() == e.getID() ) {
					// Compete in event
					System.out.print(r.compete(e));
				}
			}
		}
	}	
}