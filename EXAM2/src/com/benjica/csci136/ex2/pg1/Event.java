package com.benjica.csci136.ex2.pg1;

/**
 * Abstract event class for all track events
 * Apr 16, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public abstract class Event {

	protected int mID;
	protected String mName;
	protected double mRecord;
	protected String mUnits;
	
	/**
	 * A constructor for initialzing values
	 * @param id
	 * @param name
	 * @param record
	 * @param units
	 */
	public Event(int id, String name, double record, String units) {
		mID = id;
		mName = name;
		mRecord = record;
		mUnits = units;
	}
	
	/**
	 * A method to return a description of the event
	 * @return A string describing the event
	 */
	public abstract String description();
	
	/**
	 * A method that updates the event record
	 * @param newRecord
	 */ 
	public abstract void updateRecord(double newRecord);

	public int getID() {
		return mID;
	}

	public String getName() {
		return mName;
	}

	public double getRecord() {
		return mRecord;
	}

	public String getUnits() {
		return mUnits;
	}
}
