package com.benjica.csci136.ex2.pg1.data;

import java.util.Random;

import com.benjica.csci136.ex2.pg1.Event;

/**
 * A simple data structure for racers that allows them to compete in 
 * an event.
 * Apr 16, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Racer {
	
	private static final Random mRand = new Random();
	
	private int mRaceNumber;
	private String mName;
	private int mEventID;
	
	public Racer (int raceNumber, String name, int eventID) {
		mRaceNumber = raceNumber;
		mName = name;
		mEventID = eventID;
	}
	
	public int getRaceNumber() {
		return mRaceNumber;
	}

	public String getName() {
		return mName;
	}

	public int getEventID() {
		return mEventID;
	}
	
	public String compete(Event e) {
		 
		double amount;
		
		// Randomly choose to beat record 
		if ( mRand.nextBoolean() ) {
			// Get 98% of record and become a champion!
			amount = e.getRecord() * 0.98;
			e.updateRecord(amount);
			
			return String.format("%s has broken a record in %s with %f %s\n", mName, e.getName(), e.getRecord(), e.getUnits());
		} else {
			// Get 102% of record and loose
			amount = e.getRecord() * 1.02;
			return String.format("%s has completed in %s with %f %s\n", mName, e.getName(), e.getRecord() , e.getUnits());
		}
	}
}
