package com.benjica.csci136.ex2.pg1.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * A simple utility that rolls file parsing and array list into one
 * object.
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class RacerArrayList extends ArrayList<Racer> {
	
	private static final long serialVersionUID = 1L;

	public RacerArrayList(String filePath) throws FileNotFoundException {
		super();
		
		File f = new File(filePath);
		
		Scanner fileScanner = new Scanner(f);
		fileScanner.nextLine();
			
		while (fileScanner.hasNextLine()) {
			String[] params = fileScanner.nextLine().split(",");
			
			add(new Racer(
					Integer.parseInt(params[0]),
					params[1],
					Integer.parseInt(params[2])));
		}
		
		fileScanner.close();
		
	}
}
