package com.benjica.csci136.ex2.pg1.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import com.benjica.csci136.ex2.pg1.Event;
import com.benjica.csci136.ex2.pg1.Field;
import com.benjica.csci136.ex2.pg1.Running;

/**
 * A simple utility that rolls file parsing and array list into one
 * object.
 * Apr 16, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class EventArrayList extends ArrayList<Event> {
	
	private static final long serialVersionUID = 1L;

	public EventArrayList(String filePath) throws FileNotFoundException {
		super();
		
		File f = new File(filePath);
		
		Scanner fileScanner = new Scanner(f);
		fileScanner.nextLine();
			
		while (fileScanner.hasNextLine()) {
		
			String[] params = fileScanner.nextLine().split(",");

			switch ( params[2] ) {
			case "Running":
				add( new Running(
						Integer.parseInt(params[0]),
						params[1],
						Double.parseDouble(params[3]),
						params[4]));
				break;
			case "Field":
				add( new Field(
						Integer.parseInt(params[0]),
						params[1],
						Double.parseDouble(params[3]),
						params[4]));
				break;
			default:
				// Deal a bad data exception
			}
		}
		
		fileScanner.close();
		
	}
}
