package com.benjica.csci136.hw3.pg1;

import javax.swing.JFrame;

/**
 * This is the entry point for the histogram application
 * Mar 20, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program1 extends JFrame {

	public Program1() {
		setTitle("Program 1 - Histogram");
		
		add(new HistogramPanel());
	}
	/**
	 * The main entry point to the program.
	 * @param args
	 */
	public static void main(String[] args) {
		Program1 pg1 = new Program1();
		
		pg1.pack();
		pg1.setVisible(true);
	}
}
