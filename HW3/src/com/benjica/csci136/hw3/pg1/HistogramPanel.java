package com.benjica.csci136.hw3.pg1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * TODO Implement Class Definition for HistogramPanel.java
 * Mar 20, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class HistogramPanel extends JPanel implements ActionListener {
	
	private JButton mOpenFileButton;
	private JFileChooser mChooser;
	private File mFile;
	private CategoryDataset mDataset;
	private JFreeChart mChart;
	private ChartPanel mChartPanel;
	
	/**
	 * The constructor for  the histogram panel.
	 */
	public HistogramPanel() {
		
		// Intialize all the containers and chart panel.
		mDataset = createDataset();
        mChart = createChart(mDataset);
        mChartPanel = new ChartPanel(mChart);
		
        // Create UI components for file selection
		mOpenFileButton = new JButton("Open File");
		mChooser = new JFileChooser();
		
		// Register listener
		mOpenFileButton.addActionListener(this);
		
		// Setup layout
		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addComponent(mChartPanel)
					.addComponent(mOpenFileButton));
		
		layout.setHorizontalGroup(
				layout.createParallelGroup()
					.addComponent(mChartPanel)
					.addComponent(mOpenFileButton));
		
	}
	
	/**
	 * This creates a default set of data.
	 * @return
	 */
	private CategoryDataset createDataset() {
        DefaultCategoryDataset d = new DefaultCategoryDataset();
        
        // Create a zero value for each letter. This is important so that
        // when reading a file an increment can occur.
        d.addValue(0, "Character", "a");
        d.addValue(0, "Character", "b");
        d.addValue(0, "Character", "c");
        d.addValue(0, "Character", "d");
        d.addValue(0, "Character", "e");
        d.addValue(0, "Character", "f");
        d.addValue(0, "Character", "g");
        d.addValue(0, "Character", "h");
        d.addValue(0, "Character", "i");
        d.addValue(0, "Character", "j");
        d.addValue(0, "Character", "k");
        d.addValue(0, "Character", "l");
        d.addValue(0, "Character", "m");
        d.addValue(0, "Character", "n");
        d.addValue(0, "Character", "o");
        d.addValue(0, "Character", "p");
        d.addValue(0, "Character", "q");
        d.addValue(0, "Character", "r");
        d.addValue(0, "Character", "s");
        d.addValue(0, "Character", "t");
        d.addValue(0, "Character", "u");
        d.addValue(0, "Character", "v");
        d.addValue(0, "Character", "w");
        d.addValue(0, "Character", "x");
        d.addValue(0, "Character", "y");
        d.addValue(0, "Character", "z");
        return d;
    }

	// Generate a new histogram with labels
	private JFreeChart createChart(CategoryDataset dataset) {
		
        final JFreeChart chart = ChartFactory.createBarChart(
            "Character Histogram",
            "Character", 
            "Frequency", 
            dataset
        );
        
        return chart;    
    }
	
	// The UI action for opening files
	@Override
	public void actionPerformed(ActionEvent e) {
		
		// Test to ensure it is an open selection
		if (e.getSource() == mOpenFileButton) {
			// Is it a valid selection?
			if ( mChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION ) {
				// Grab file
				mFile = mChooser.getSelectedFile();
							
				// Better try an open it
				try {
					Scanner fileScanner = new Scanner(mFile);					
					
					// Iterate over lines
					while (fileScanner.hasNextLine()) {
						String line = fileScanner.nextLine();
						// Blow up line and iterate over chars
						for ( char c : line.toCharArray() ) {
							// Only match letters
							if ( String.valueOf(c).matches("[a-zA-Z]") ) {
								// Increment the value in the dataset, lowercase all characters to make the set case
								// insensitive
								((DefaultCategoryDataset) mDataset).addValue(
									((DefaultCategoryDataset) mDataset).getValue("Character", String.valueOf(c).toLowerCase()).intValue() + 1,
									"Character",
									String.valueOf(c).toLowerCase());
							}
						}
					}
					
				} catch (FileNotFoundException e1) {
					// Just give up on life.
					e1.printStackTrace();
				}
			}
		}
	}
}
