package com.benjica.csci136.hw3.pg3;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Lets roll our own dynamic array.
 * Mar 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program3 {

	public static void main(String[] args) throws IOException {
		// Create our container and utility classes
		FileArray list = new FileArray(args[0]);
		Scanner userInput = new Scanner(System.in);
		
		// Print UI structure
		System.out.printf("**************************************************\n");
		System.out.printf("*                Television Shows                *\n");
		System.out.printf("**************************************************\n\n");
		
		do {
			
			System.out.printf("Please choose an option below:\n");
			System.out.printf("p) Print Current List\n");
			System.out.printf("a) Add an item to the list\n");
			System.out.printf("s) Save the list\n");
			
			try {
				
				// Catch user input
				switch( userInput.next("(?i)[pas]").toLowerCase().charAt(0) ) {
				case 'p':
					System.out.printf("                 Television Shows                   \n");
					System.out.printf("**************************************************\n\n");
					
					for ( int i = 0; i < list.size(); i++ ) {
						System.out.printf("* %d) %s\n", i+1, list.get(i));
					}
					
					System.out.printf("**************************************************\n\n");
					break;
				case 'a':
					userInput.nextLine();
					System.out.printf("                      Add Show                   \n");
					System.out.printf("**************************************************\n\n");
					System.out.printf("Please enter a new show:\n");
					
					list.add(userInput.nextLine());
					break;
					
				case 's':
					System.out.printf("                     Saving List                   \n");
					System.out.printf("**************************************************\n\n");
					
					try {
						list.saveFile();
					} catch (FileNotFoundException e) {
						System.out.printf("There was an issue when saving the list. The file has not been saved.\n%s\n", e.getLocalizedMessage());
					}
					break;
				}
				
			} catch (InputMismatchException e) {
				// If they input a non defined value, then alert them.	
				System.err.printf("\n\nInvalid Selection!\n\n");
				userInput.nextLine();
			}
			
		} while ( true );
		
	}
}
