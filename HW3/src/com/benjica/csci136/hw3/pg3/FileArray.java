package com.benjica.csci136.hw3.pg3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * A homemade arraylist
 * Mar 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class FileArray {
	// Constants
	private final static int INITIAL_SIZE = 2;
	
	// Container fields
	private String[] mContainer;
	private int mCursor;
	
	// File fields
	private File mFile;
	private Scanner mScanner;
	
	/**
	 * This creates a new list by loading a given path and loading entries
	 * @param path
	 * @throws IOException
	 */
	public FileArray(String path) throws IOException {
		// Create contianer
		mContainer = new String[INITIAL_SIZE];
		mCursor = 0;
		
		// Try to load the file
		mFile = new File(path);
		try {
			loadFile();
		} catch (FileNotFoundException e ) {
			// If the given file does not exist creat it and load an empty file
			mFile.createNewFile();
			loadFile();
		}
	}
	
	/**
	 * This adds and element to the container.
	 * @param item
	 */
	public void add(String item) {
		try {
			// We try to insert at the cursor.
			mContainer[mCursor++] = item;
		} catch (IndexOutOfBoundsException e ) {
			// But if the cursor is beyond the edge of the container deincrement
			mCursor--;
			// blow contianer up
			growContainer();
			// And try again
			add(item);
		}
		
	}
	
	/**
	 * Graps the indexed element
	 * @param index
	 * @return
	 */
	public String get(int index) {
		return mContainer[index];
	}
	
	/**
	 * The cursor position reveals the current stored size.
	 * @return
	 */
	public int size() {
		// The container may have more space avaialable but this is nessecary
		// if we want to iterate over supplied items.
		return mCursor;
	}
	
	/**
	 * A helper function for loading an exisitng list
	 * @throws FileNotFoundException
	 */
	public void loadFile() throws FileNotFoundException {
		
		// Utility
		mScanner = new Scanner(mFile);
		
		// Iterate over lines
		while (mScanner.hasNextLine() ) {
			// Add line to container
			add(mScanner.nextLine());
		}
		//Cleanup
		mScanner.close();

	}
	
	/**
	 * A hleper function for saving current list
	 * @throws FileNotFoundException
	 */
	public void saveFile() throws FileNotFoundException {
		// Open file for writing, with overwrite so list is conanoical
		PrintWriter w = new PrintWriter( mFile );
		
		// Iterate over contianer
		for ( int i = 0; i < mCursor; i++) {
			// Write element
			w.printf("%s\n", mContainer[i]);
		}
		// Cleanup
		w.close();
	}
	
	/** 
	 * A helpef function to expand container.
	 */
	private void growContainer() {
		// Create a new container twice as large
		String[] newContainer = new String[mContainer.length * 2];
		
		// Iterate over current stored and place in new
		for ( int i = 0; i < mCursor; i++ ) {
			newContainer[i] = mContainer[i];
		}
		
		// Replace container
		mContainer = newContainer;
	}
}
