package com.benjica.csci136.hw3.pg2;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.jdom2.JDOMException;

/**
 * TODO Implement Class Definition for Program2.java
 * Mar 22, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program2 {

	private static Scanner mIScanner;
	private static final String PANEL_DIVIDER = "**************************************************";

	public static void main(String[] args) throws JDOMException, IOException {
		
		mIScanner = new Scanner(System.in);
	
		// Load in story
		File input = new File("story.xml");
		StoryNode root = StoryNode.createStoryFromXML(input);
		
		// We iterate over the story tree as we traverse it.
		do {
			
			// This prints a kind o pretty multiline format of the prompt
			print_lines(root.getPrompt());
			System.out.printf("*         ------ Make your choice ------         *\n");
			System.out.printf("*                                                *\n");
			
			// Browse the leaves for choices
			int i = 1;
			for ( String choice : root.getChoices() ) {				System.out.printf("* %d) %-43s *\n", i++, choice);
			}
			System.out.printf("%s\n\n", PANEL_DIVIDER);
			// Block to user to get response
			int answer = mIScanner.nextInt();
			root = root.choose(answer);
			
		} while (!root.isEnd()); // If this is the end of the tree then break out
		 
		// Print the last prompt
		print_lines(root.getPrompt());
		System.out.println("                    !THE END!");
	} 
	
	/**
	 * A helper method to print a multiline string
	 * @param in
	 */
	private static void print_lines(String in) {
		// Setup ui
		System.out.printf("%s\n", PANEL_DIVIDER);
		//Chop the string so that it fits in the box
		for ( int i = 0; i < (in.length() / 46) + 1; i++ ) {
			// Print a substring of 46 char blocks.
			System.out.printf("* %-46s *\n", in.subSequence(i * 46,
					46 + i * 46 > in.length() ? in.length() : 46 + i * 46));
		}
		System.out.printf("%s\n", PANEL_DIVIDER);
	}
	
}
