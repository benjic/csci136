package com.benjica.csci136.hw3.pg2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * This represents a tree structure in which players make choices to traverse
 * the tree and create a story.
 * Mar 22, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class StoryNode {
	
	// Class fields
	private String mChoice;
	private String mPrompt;
	private ArrayList<StoryNode> mChoices;
	
	// Default Constructor
	public StoryNode() {
		mChoices = new ArrayList<StoryNode>();
		mChoice = "";
		mPrompt = "";
	}
	
	/**
	 * A test to see if this is a leaf
	 * @return
	 */
	public boolean isEnd() {
		// If you have choices, you are not at the end
		return (mChoices.size() < 1);
	}
	
	
	public String getPrompt() {
		return mPrompt;
	}
	
	/**
	 * Get a list of choices for a branch
	 * @return
	 */
	public List<String> getChoices() {
		List<String> p = new ArrayList<String>();
		
		for (StoryNode n : mChoices) {
			p.add(n.mChoice);
		}
		
		return p;
	}
	
	/**
	 * A static method to parse an xml document and recursively create the tree
	 * @param input
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	static StoryNode createStoryFromXML(File input) throws JDOMException, IOException {
		
		// Setup JDOM parts
		SAXBuilder builder = new SAXBuilder();
		Document document = (Document) builder.build(input);
		Element rootNode = document.getRootElement();
	
		// Make a root node
		StoryNode root = parseNode( rootNode.getChild("node") );
		
		return root;
		
	}
	
	/**
	 * A helper method to recursively parse xml
	 * @param node
	 * @return
	 */
	private static StoryNode parseNode(Element node) {
		// Create a new branch
		StoryNode leaf = new StoryNode();
		
		// Parse the branch properties
		leaf.mChoice = node.getChildText("choice");
		leaf.mPrompt = node.getChildText("prompt");
		
		// If it has a nodes element recurse the choices
		if ( node.getChild("nodes") != null ) {
			for ( Element child : node.getChild("nodes").getChildren("node") ) {
				// Add a parsed node for any choice nodes
				leaf.mChoices.add(parseNode(child));
			}
		}
		return leaf;
	}

	/**
	 * This allows you to update the pointer to the current leaf
	 * @param answer
	 * @return
	 */
	public StoryNode choose(int answer) {
		// Grab the n-1 choice node
		return mChoices.get(answer - 1);
	}
}
