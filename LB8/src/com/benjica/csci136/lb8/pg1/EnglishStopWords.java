package com.benjica.csci136.lb8.pg1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;

/**
 * TODO Implement Class Definition for EnglishStopWords.java
 * May 9, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class EnglishStopWords extends HashSet<String> {
	
	private static final String[] FILES = {
		"data/pg1/stop-words/stop-words_english_1_en.txt",
		"data/pg1/stop-words/stop-words_english_2_en.txt",
		"data/pg1/stop-words/stop-words_english_3_en.txt",
		"data/pg1/stop-words/stop-words_english_4_google_en.txt",
		"data/pg1/stop-words/stop-words_english_5_en.txt",
		"data/pg1/stop-words/stop-words_english_6_en.txt",};
	
	private static final String[] CUSTOM_STOP_WORDS = {
		"",
	};
	
	public EnglishStopWords() {
		super(); 
		
		for(String path : FILES ) {
		
				try {
					
					Scanner filescanner = new Scanner(new File(path));
					
					while ( filescanner.hasNextLine()) {
						
						add(filescanner.nextLine().trim());
					}
					
					filescanner.close();
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
		}
		
		for (String word : CUSTOM_STOP_WORDS) {
			add(word);
		}
	}
}
