package com.benjica.csci136.lb8.pg1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;


/**
 * TODO Implement Class Definition for Program1.java
 * May 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program1 {

	// Yeah!
	private static final EnglishStopWords sStopWords = new EnglishStopWords();
	
	public static void main(String[] args) throws FileNotFoundException {

		// Utility Classes
		Scanner fileScanner = new Scanner(new File("data/pg1/pg215.txt"));
		Map<String, Integer> wordmap = new HashMap<String, Integer>();
		
		// Iterate through file
		while ( fileScanner.hasNextLine() ) {
			
			Set<String> words = new HashSet<String>(Arrays.asList(	// Build a set of words
					fileScanner.nextLine() 							// Get the line
					.replaceAll("[^a-zA-Z0-9\\s]", "")				// Only allow desirable characters
					.trim()											// Remove end of line (I think is redundant after the previous line)
					.toLowerCase()									// Remove case
					.split(" ")));									// Partition into words
			
			
			words.removeAll(sStopWords);							// This is why the set was important, I am now able to perform an
																	// Asymmetric difference between the words and all of the stop words
																	// I believe with the HashSets this is super fast comparisons !!!
			
			
			for ( String word : words ) {							// Iterate over word set
				if ( wordmap.containsKey(word) )					// If word is in map
					wordmap.put(word, wordmap.get(word) + 1);		// increment
				else 												// otherwise
					wordmap.put(word, 1);							// insert into map
			}
			
		}
		
		fileScanner.close();
		
		// Maps are not sortable, but a list of the map entries is!
		List<Entry<String, Integer>> wordlist = new LinkedList<Entry<String, Integer>>(wordmap.entrySet());
		
		// Lists of map entries do not have a natural order so in order to sort 
		// we have to define a comparator.
		Collections.sort(wordlist, new Comparator<Entry<String, Integer>>() {

			@Override
			public int compare(Entry<String, Integer> a, Entry<String, Integer> b) {
				return b.getValue() - a.getValue(); 				// With integers I can just return the difference which yields the right
																	// signum for comparisons.
			}
			
		});
		
		
		// Print Results
		System.out.println("The top ten most frequent words in your file:");
		for (int i = 0; i < 10; i++ ) {
			System.out.printf("%s: %d\n", wordlist.get(i).getKey(), wordlist.get(i).getValue() );
		}
	}
	
}
