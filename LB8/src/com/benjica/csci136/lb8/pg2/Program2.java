package com.benjica.csci136.lb8.pg2;

/**
 * TODO: VIN Verification Program
 **/
public class Program2 {

	public static void main(String[] args) {
		
		// Iterate over console parameters
		for(String arg : args ) {
			System.out.println(String.format("The VIN %s is %svalid.", arg, VIN.validate(arg) ? "" : "not "));
		}
	}
}
