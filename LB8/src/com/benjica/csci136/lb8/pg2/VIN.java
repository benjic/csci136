package com.benjica.csci136.lb8.pg2;

public class VIN {

	// Value and Weight maps defined for VINs
	private static final int[] VALUES = { 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2, 3, 4, 5, 0, 7, 0, 9, 2, 3, 4, 5, 6, 7, 8, 9 };
	private static final int[] WEIGHTS = { 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 };

	public static boolean validate(String VINNumber) {

		
		String cleanVIN = VINNumber.replaceAll("-", "").toUpperCase();
		
		if ( cleanVIN.length() != 17 ) 
			throw new RuntimeException("VIN number must be 17 characters");
		
		switch ( checksum(cleanVIN, 17) % 11 ) {
			case 10:
				return cleanVIN.charAt(8) == 'X';
			default:
				return checksum(cleanVIN, 17) % 11 == cleanVIN.charAt(8) - '0';
		}
	}

	private static int checksum(String VINNumber, int d) {
		
		// If you reach the end of your string stop
		if ( VINNumber.length() == 0  )
			return 0;
		
		// Convince vars
		char c = VINNumber.charAt(0);
		int  w = -1 * (d - 17);
		
		if ( d == 9 )
			if ( c == 'X' || ( c >= '0' && c <= '9' ) )
				return checksum(VINNumber.substring(1), d-1); 				// Do not add the check digit
			else
				throw new RuntimeException("Illegal check digit: " + c);	// Surely, die if you are not a valid digit
		 
		if ( c >= 'A' && c <= 'Z')
			return (VALUES[c - 'A'] * WEIGHTS[w]) + checksum(VINNumber.substring(1), d-1);
		
		if ( c >= '0' && c <= '9') 
			return ((c - '0') * WEIGHTS[w]) + checksum(VINNumber.substring(1), d-1);
		
		// If you don't match the accepted characters, your gonna have a bad time.
		throw new RuntimeException(String.format("Invalid Character: %c", c) );
	}
}
