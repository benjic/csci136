package com.benjica.csci136.hw2.pg4;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * TODO Implement Class Definition for GamePanel.java
 * Feb 25, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class GamePanel extends JPanel {
	
	Random mRand = new Random();
	
	JLabel mLowerBoundLabel = new JLabel("Lower Limit:");
	JLabel mUpperBoundLabel = new JLabel("Upper Limit:");
	JLabel mNumberOfQuestionsLabel = new JLabel("Number of Questions:");
	JLabel mQuestionLabel = new JLabel();
	
	JTextField mLowerBoundText = new JTextField(5);
	JTextField mUpperBoundText = new JTextField(5);
	JTextField mNumberOfQuestionsText = new JTextField(5);
	
	JButton mAnswer1Button = new JButton();
	JButton mAnswer2Button = new JButton();
	JButton mAnswer3Button = new JButton();
	
	JButton mEndGame = new JButton("Reset");
	JButton mStartGame = new JButton("Start");
	
	int mA = 0;
	int mB = 0;
	int mAnswer = 0;
	
	public GamePanel() {
		
		mLowerBoundText.setText("-10");
		mUpperBoundText.setText("10");
		mNumberOfQuestionsText.setText("10");
		
		mQuestionLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 64));
		mQuestionLabel.setText("a - b = ?");
		
		mStartGame.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				deactivateParameters();
			}
			
		});
		
		mEndGame.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				activateParameters();
			}
			
			
		});
		
		mAnswer1Button.addActionListener(new AnswerButtonActionListener(this));
		mAnswer2Button.addActionListener(new AnswerButtonActionListener(this));
		mAnswer3Button.addActionListener(new AnswerButtonActionListener(this));
		
		activateParameters();
		
		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setHorizontalGroup(
				layout.createParallelGroup(Alignment.CENTER)
					.addGroup(
							layout.createSequentialGroup()
								.addComponent(mLowerBoundLabel)
								.addComponent(mLowerBoundText)
								.addComponent(mUpperBoundLabel)
								.addComponent(mUpperBoundText)
								.addComponent(mNumberOfQuestionsLabel)
								.addComponent(mNumberOfQuestionsText)
					).addGroup(
							layout.createSequentialGroup()
								.addComponent(mQuestionLabel)
					).addGroup(
							layout.createSequentialGroup()
								.addComponent(mAnswer1Button)
								.addComponent(mAnswer2Button)
								.addComponent(mAnswer3Button)
					).addGroup(
							layout.createSequentialGroup()
								.addComponent(mEndGame)
								.addComponent(mStartGame))
		);
		
		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addGroup(
							layout.createParallelGroup()
								.addComponent(mLowerBoundLabel)
								.addComponent(mLowerBoundText)
								.addComponent(mUpperBoundLabel)
								.addComponent(mUpperBoundText)
								.addComponent(mNumberOfQuestionsLabel)
								.addComponent(mNumberOfQuestionsText)
					).addGroup(
							layout.createParallelGroup()
								.addComponent(mQuestionLabel)
					).addGroup(
							layout.createParallelGroup()
								.addComponent(mAnswer1Button)
								.addComponent(mAnswer2Button)
								.addComponent(mAnswer3Button)
					).addGroup(
							layout.createParallelGroup()
								.addComponent(mEndGame)
								.addComponent(mStartGame))
		);
	}
	
	public void deactivateParameters() {
		mLowerBoundText.setEditable(false);
		mUpperBoundText.setEditable(false);
		mNumberOfQuestionsText.setEditable(false);
		
		mAnswer1Button.setVisible(true);
		mAnswer2Button.setVisible(true);
		mAnswer3Button.setVisible(true);
		
		buildQuestion();
	}
	
	public void activateParameters() {
		mLowerBoundText.setEditable(true);
		mUpperBoundText.setEditable(true);
		mNumberOfQuestionsText.setEditable(true);
		mNumberOfQuestionsText.setText("10");
		
		mAnswer1Button.setVisible(false);
		mAnswer2Button.setVisible(false);
		mAnswer3Button.setVisible(false);
	}
	
	public void buildQuestion() {
		int uppperBound = Integer.parseInt(mUpperBoundText.getText());
		int lowerBound = Integer.parseInt(mLowerBoundText.getText());
		
		mA = mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound;
		mB = mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound;
		
		mAnswer = mA - mB;
		
		switch( mRand.nextInt(3)) {
		case 0:
			mAnswer1Button.setText(String.format("%d", mAnswer));
			mAnswer2Button.setText(String.format("%d", mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound));
			mAnswer3Button.setText(String.format("%d", mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound));
			break;
		case 1:
			mAnswer2Button.setText(String.format("%d", mAnswer));
			mAnswer1Button.setText(String.format("%d", mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound));
			mAnswer3Button.setText(String.format("%d", mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound));
			break;
		default:
			mAnswer3Button.setText(String.format("%d", mAnswer));
			mAnswer2Button.setText(String.format("%d", mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound));
			mAnswer1Button.setText(String.format("%d", mRand.nextInt( ( uppperBound - lowerBound) + 1 ) + lowerBound));
		}
		
		mQuestionLabel.setText(String.format("(%d) - (%d) = ?", mA, mB));
	}
	
	public void answerQuestion(int answer) {
				
		if ( answer == mAnswer ) {
			this.mNumberOfQuestionsText.setText(String.format("%d",Integer.parseInt(mNumberOfQuestionsText.getText()) - 1));
			if ( Integer.parseInt(mNumberOfQuestionsText.getText())  <= 0 ) {
				activateParameters();
			} else {
				buildQuestion();
			}
		}
		
	}
	
	public class AnswerButtonActionListener implements ActionListener {
		
		GamePanel mGamePanel;
		
		public AnswerButtonActionListener(GamePanel g) {
			mGamePanel = g;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton b = (JButton) e.getSource();
			mGamePanel.answerQuestion(Integer.parseInt(b.getText()));
			
		}
		
	}
	
}
