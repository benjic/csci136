package com.benjica.csci136.hw2.pg4;

import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 * TODO Implement Class Definition for Program4.java
 * Feb 24, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program4 extends JFrame {
	
	public Program4() {
		setTitle("Program 4");
		setPreferredSize(new Dimension(600,220));
		
		add(new GamePanel());
		
	}
	
	public static void main(String[] args) {
		Program4 pg4 = new Program4();
		
		pg4.pack();
		pg4.setVisible(true);
	}
}
