package com.benjica.csci136.hw2.pg3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * TODO Implement Class Definition for OscarBestPictureTableModel.java
 * Feb 24, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class OscarBestPictureTableModel implements TableModel {
	
	private Film[] mBestPictures;
	
	public OscarBestPictureTableModel() throws FileNotFoundException {
		// Initialize
		mBestPictures = new Film[10];
		
		File file = new File("./data/bestPictures");
		Scanner fileScanner = new Scanner(file);
		
		for (int i = 0; i < mBestPictures.length; i++ ) {
			String[] params = fileScanner.nextLine().split("\\|");
			Film f = new Film();
			
			f.Title = params[0];
			f.Genre = params[1];
			f.Rating = params[2];
			f.RunningTime = Integer.parseInt(params[3]);
			f.Favorite = Integer.parseInt(params[4]) > 0 ? true : false;
			
			mBestPictures[i] = f;
		}
		
		fileScanner.close();
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		//NOOP Model doesn't change
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		
		switch ( columnIndex ) {
		case 0:
		case 1:
		case 2:
			return String.class;
		case 3:
			return Integer.class;
		case 4:
			return Boolean.class;
		default:
			return null;
		}
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex) {
		case 0:
			return "Title";
		case 1:
			return "Genre";
		case 2:
			return "Rating";
		case 3:
			return "Running Time (min)";
		case 4:
			return "Favorite";
		default:
			return null;
		}
	}

	@Override
	public int getRowCount() {
		return 10;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex) {
		case 0:
			return mBestPictures[rowIndex].Title;
		case 1:
			return mBestPictures[rowIndex].Genre;
		case 2:
			return mBestPictures[rowIndex].Rating;
		case 3:
			return mBestPictures[rowIndex].RunningTime;
		case 4:
			return mBestPictures[rowIndex].Favorite;
		default:
			return null;
		}
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0) {
		//NOOP Table doesn't change
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2) {
		// NOOP Data is immutable
	}
	
}
