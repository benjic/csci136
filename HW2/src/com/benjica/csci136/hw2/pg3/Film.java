package com.benjica.csci136.hw2.pg3;

/**
 * TODO Implement Class Definition for Film.java
 * Feb 24, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Film {
	public String Title;
	public String Genre;
	public String Rating;
	public int RunningTime;
	public boolean Favorite;
}
