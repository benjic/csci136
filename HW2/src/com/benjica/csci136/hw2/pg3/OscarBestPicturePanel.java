package com.benjica.csci136.hw2.pg3;

import java.io.FileNotFoundException;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * TODO Implement Class Definition for OscarBestPicturePanel.java
 * Feb 24, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class OscarBestPicturePanel extends JPanel {
	
	public OscarBestPicturePanel() throws FileNotFoundException {
		
		JTable mOscarTable = new JTable(new OscarBestPictureTableModel());
		JScrollPane mPane = new JScrollPane(mOscarTable);
		
		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(
				layout.createParallelGroup()
					.addComponent(mPane));
		
		layout.setVerticalGroup(
				layout.createParallelGroup()
					.addComponent(mPane));
	}
}
