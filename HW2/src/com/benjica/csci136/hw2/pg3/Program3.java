package com.benjica.csci136.hw2.pg3;

import java.io.FileNotFoundException;

import javax.swing.JFrame;

/**
 * TODO Implement Class Definition for Program3.java
 * Feb 24, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program3 extends JFrame {

	public Program3() throws FileNotFoundException {
		setTitle("Program 3");
		OscarBestPicturePanel oPanel = new OscarBestPicturePanel();
		
		add(oPanel);
		
	}
	
	public static void main(String[] args) {
		
		Program3 pg3;
		try {
			pg3 = new Program3();
			
			pg3.pack();
			pg3.setVisible(true);
		} catch (FileNotFoundException e) {
			System.err.printf("The default file is funky.\n");
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
}
