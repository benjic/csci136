package com.benjica.csci136.hw2.pg2;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * TODO Implement Class Definition for Order.java
 * Feb 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Order extends ArrayList<Item> implements TableModel {
	
	private String mDate;
	private TableModelListener mListener;
	
	public Order() {;
		mDate = new Date().toLocaleString();
		
	}
	
	public double getTotal() {
		
		double total = 0;
		
		for( Item i : this ) {
			total += i.Price * i.Quanity;
		}
		
		return total;
		
	}
	
	public int getNumberOfItems() {
		
		int total = 0;
		
		for( Item i : this ) {
			total +=  i.Quanity;
		}
		
		return total;
	}
	
	public String getDate() {
		return mDate;
	}


	@Override
	public Class<?> getColumnClass(int columnIndex) {
		
		switch ( columnIndex ) {
		case 0:
			return String.class;
		case 1:
			return Integer.class;
		case 2:
			return Double.class;
		default:
			return null;
		}
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int columnIndex) {
		
		switch( columnIndex ) {
		case 0:
			return "Name";
		case 1:
			return "Quanity";
		case 2:
			return "Price";
		default:
			return "Invalid";
		}
		
	}

	@Override
	public int getRowCount() {
		
		return size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		Item i = get(rowIndex);
		
		switch ( columnIndex ) {
		case 0:
			return i.Name;
		case 1:
			return i.Quanity;
		case 2:
			return i.Price;
		default:
			return null;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		
		return true;
	}

	@Override
	public boolean add(Item e) {
		// TODO Auto-generated method stub
		mListener.tableChanged(new TableModelEvent(this));
		return super.add(e);
	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		mListener = l;
		
	}
	
	@Override
	public void removeTableModelListener(TableModelListener l) {
		mListener = null;
		
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		
		Item i = get(rowIndex);
		
		switch ( columnIndex ) {
		case 0:
			i.Name = (String) aValue;
			break;
		case 1:
			i.Quanity = (Integer) aValue;
			break;
		case 2:
			i.Price = (Double) aValue;
		}
		
	}
}
