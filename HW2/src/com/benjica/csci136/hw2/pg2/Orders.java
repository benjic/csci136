package com.benjica.csci136.hw2.pg2;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * TODO Implement Class Definition for Orders.java
 * Feb 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Orders extends ArrayList<Order> implements TableModel {

	TableModelListener mTableModelListener;
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch( columnIndex ) {
		case 0:
			return String.class;
		case 1:
			return Integer.class;
		case 2:
			return String.class;
		default:
			return null;
		}
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public boolean add(Order e) {
		
		mTableModelListener.tableChanged(new TableModelEvent(this));
		
		return super.add(e);
	}

	@Override
	public String getColumnName(int columnIndex) {
		
		String[] names = { "Date", "Number of Items", "Total" };
		
		return names[columnIndex];
	}

	@Override
	public int getRowCount() {
		return size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Order o = get(rowIndex);
		
		switch ( columnIndex ) {
		case 0:
			return o.getDate();
		case 1:
			return o.getNumberOfItems();
		case 2:
			NumberFormat cf =  NumberFormat.getCurrencyInstance();
			return cf.format(o.getTotal());
		default:
			return null;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// This list information is an aggregation of sublists so it should not
		// editable.
	}
	
	@Override
	public void removeTableModelListener(TableModelListener l) {
		mTableModelListener = null;

	}
	
	@Override
	public void addTableModelListener(TableModelListener l) {
		mTableModelListener = l;

	}
	
}
