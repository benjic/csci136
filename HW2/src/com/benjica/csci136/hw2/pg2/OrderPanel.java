package com.benjica.csci136.hw2.pg2;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * TODO Implement Class Definition for OrderPanel.java
 * Feb 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class OrderPanel extends JPanel {
	
	private Orders mOrderModel;
	private Order mActiveOrder;
	

	public OrderPanel() {
		
		setPreferredSize(new Dimension(800,600));
		
		Order o = new Order();
		
		
		JLabel mOrderLabel = new JLabel("Orders");
		JLabel mItemLabel = new JLabel("Line Items");
		mActiveOrder = o;
		mOrderModel = new Orders();
		JTable mOrderTable = new JTable(mOrderModel);
		final JTable mItemTable = new JTable(o);
		
		mOrderTable.getSelectionModel()
			.addListSelectionListener( 
					new ListSelectionListener() {
						@Override
						public void valueChanged(ListSelectionEvent e) {

							System.err.printf("%d : %d \n", e.getFirstIndex(), e.getLastIndex());
							mItemTable.setModel(mOrderModel.get(e.getFirstIndex()));
				
						}
					});
		
		
		
		
		
		JButton mAddOrderButton = new JButton("Add Order");
		mAddOrderButton.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				mOrderModel.add(new Order());
				
			}
			
		});
		
		JButton mAddLineItemButton = new JButton("Add Item");
		mAddLineItemButton.addActionListener( new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Order o = (Order) mItemTable.getModel();
				
				o.add(new Item());
				
			}
			
		});
		
		JScrollPane oPane = new JScrollPane(mOrderTable);
		JScrollPane iPane = new JScrollPane(mItemTable);
		
		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		
		layout.setAutoCreateContainerGaps(true);
		layout.setAutoCreateGaps(true);
		
		layout.setHorizontalGroup(
				layout.createParallelGroup()
					.addComponent(mOrderLabel)
					.addGroup(
							layout.createParallelGroup(Alignment.TRAILING)
								.addComponent(oPane)
								.addComponent(mAddOrderButton)
					)
					.addComponent(mItemLabel)
					.addGroup(
							layout.createParallelGroup(Alignment.TRAILING)
								.addComponent(iPane)
								.addComponent(mAddLineItemButton)
					));
		
		layout.setVerticalGroup(
				layout.createSequentialGroup()
					.addComponent(mOrderLabel)
					.addComponent(oPane)
					.addComponent(mAddOrderButton)
					.addComponent(mItemLabel)
					.addComponent(iPane)
					.addComponent(mAddLineItemButton));
		
	}
}
