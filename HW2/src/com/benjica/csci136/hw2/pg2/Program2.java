package com.benjica.csci136.hw2.pg2;

import java.util.ArrayList;

import javax.swing.JFrame;

/**
 * TODO Implement Class Definition for Program2.java
 * Feb 23, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Program2 extends JFrame {

	public Program2() {
		
		OrderPanel op = new OrderPanel();
		
		add(op);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Program2 p2 = new Program2();
		
		p2.pack();
		p2.setVisible(true);
	}
	
}
