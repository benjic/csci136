package com.benjica.csci136.hw2.pg1;

import java.io.Serializable;

public class Donation implements Serializable {

	private Donor mDonor;
	private String mDate;
	private String mType;
	private double mValue;
	
	public void setDonor(Donor d) {
		mDonor = d;
	}

	public void setDate(String date) {
		mDate = date;
		
	}

	public void setType(String type) {
		mType = type;
		
	}

	public void setValue(double value) {
		mValue = value;
		
	}
	
	public String toString() {
		return String.format("%s donated %f on %s by %s",
								mDonor.getName(),
								mValue,
								mDate,
								mType);
	}

	public String getDate() {
		// TODO Auto-generated method stub
		return mDate;
	}

	public Double getValue() {
		// TODO Auto-generated method stub
		return mValue;
	}

	public String getType() {
		// TODO Auto-generated method stub
		return mType;
	}
	
}
