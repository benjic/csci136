package com.benjica.csci136.hw2.pg1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A wrapper class to add persistence to an List. This allows
 * lists to exist beyond each execution. This could totally be expanded
 * to be more efficient and used time or count based writes to save IO.
 * 
 * @author Benjaimn Campbell <benjamin.campbell@mso.umt.edu>
 *
 * @param <E>
 */
public class FileList<E> implements  Iterable<E> {

	private ArrayList<E> mObjects;
	private String mPath;
	
	/**
	 * Constructor requires a path to where the serialization file will be 
	 * saved.
	 * @param filePath
	 */
	public FileList(String filePath) {

		mPath = filePath;
		try {
			InputStream file = new FileInputStream(mPath);
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream (buffer);
			
			mObjects = (ArrayList<E>) input.readObject();
			
			input.close();
		} catch (Exception ie) {
			// If file is unavailable create a new list
			mObjects = new ArrayList<E>();
		}
	}
	
	/**
	 * Add a new object and save to file.
	 */
	public void add(E e) {
		
		mObjects.add(e);
		
		persist();
	}
	
	/**
	 * This takes t
	 */
	public void persist() {
		try {
			OutputStream file = new FileOutputStream(mPath);
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);
			      
			output.writeObject(mObjects);
			
			output.close();
		}  catch(IOException oe) {
			   oe.printStackTrace(); 	
		}
	}

	@Override
	public Iterator<E> iterator() {
		return mObjects.iterator();
	}
	
	public E get(int index) {
		return mObjects.get(index);
	}
}
