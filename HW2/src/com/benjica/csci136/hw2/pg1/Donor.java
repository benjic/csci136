package com.benjica.csci136.hw2.pg1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Donor implements Serializable {

	public class Address implements Serializable {
		public String Street1;
		public String Street2;
		public String City;
		public String State;
		public String Zip;
	}
	
	private String mName;
	private Address mAddress;
	private ArrayList<Donation> mDonations;
	
	public Donor() {
		mAddress = new Address();
		mDonations = new ArrayList<Donation>();
	}
	
	public Address getAddress() {
		return mAddress;
	}

	public void setName(String name) {
		mName = name;
	}
	
	public String toString() {
		return String.format("%s\n%s\n%s\n%s %s %s",
									mName,
									mAddress.Street1,
									mAddress.Street2,
									mAddress.City,
									mAddress.State,
									mAddress.Zip);
	}

	public void addDonation(Donation d) {
		mDonations.add(d);
	}
	
	public List<Donation> getDonations() {
		return mDonations;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return mName;
	}
}
