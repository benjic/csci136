package com.benjica.csci136.hw2.pg1;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A simple donation program.
 * @author benjica
 *
 */
public class Program1 {

	/**
	 * Entry point into the program.
	 * @param args
	 */
	public static void main(String[] args) {
		
		FileList<Donor> donors = new FileList<Donor>("data/donors");
		FileList<Donation> donations = new FileList<Donation>("data/donations");
		
		Scanner userInput = new Scanner(System.in);
		
		System.out.printf("**************************************************\n");
		System.out.printf("*                Donations Tracker               *\n");
		System.out.printf("**************************************************\n\n");
		
		boolean continueDonationsTracker = true;
		
		do {
			System.out.printf("Please choose an option below:\n");
			System.out.printf("d) Donor Utilities\n");
			System.out.printf("n) Donation Utilities\n");
			System.out.printf("x) Generate Reports\n");
			
			try {
				
				switch( userInput.next("(?i)[dnx]").toLowerCase().charAt(0) ) {
				case 'd':
					System.out.printf("                  Donor Utilties                  \n");
					System.out.printf("**************************************************\n\n");
					
					boolean continueDonorUtility = true;
					
					do {
						
						System.out.printf("Please choose an option below:\n");
						System.out.printf("a) Add Donor\n");
						System.out.printf("l) List Donors\n");
						System.out.printf("i) Get itemized list by donor\n");
						System.out.printf("x) Exit Donor Utilities\n");
						
						
						try {
							
							switch( userInput.next("(?i)[alix]").toLowerCase().charAt(0) ) {
							case 'a':
								userInput.nextLine();
								System.out.printf("                     Add Donor                    \n");
								System.out.printf("**************************************************\n\n");
								Donor d = new Donor();								
								
								System.out.printf("Donor's Name: ");
								d.setName(userInput.nextLine());
								
								System.out.printf("Donor's Street1: ");
								d.getAddress().Street1 = userInput.nextLine();
								
								System.out.printf("Donor's Street2: ");
								d.getAddress().Street2 = userInput.nextLine();
								
								System.out.printf("Donor's City: ");
								d.getAddress().City = userInput.nextLine();
								
								System.out.printf("Donor's State: ");
								d.getAddress().State = userInput.nextLine();
								
								System.out.printf("Donor's Zip: ");
								d.getAddress().Zip = userInput.nextLine();
								
								donors.add(d);
								
								System.out.printf("Donor successfully added!\n%s\n", d);
								
								break;
							case 'l':
								userInput.nextLine();
								System.out.printf("                    List Donors                   \n");
								System.out.printf("**************************************************\n\n");
								
								int count = 0;
								
								for( Donor donor : donors) {
									System.out.printf("Donor #%d\n", count++);
									System.out.printf("**************************************************\n");
									System.out.printf("%s\n\n", donor);
								}
								break;
								
							case 'i':
								userInput.nextLine();
								System.out.printf("                Itemized By Donors                \n");
								System.out.printf("**************************************************\n\n");
								
								int itemDonorCount = 0;
								System.out.printf("Please choose a donor to itemize:\n");
								for( Donor donor : donors) {
									System.out.printf("Donor #%d - %s \n", itemDonorCount++, donor.getName());
								}
								
								System.out.printf("Donor's ID: ");
								Donor donor = donors.get(Integer.parseInt(userInput.nextLine()));
								
								System.out.printf("**************************************************\n\n");
								System.out.printf("%s\n\n", donor);
								System.out.printf("**************************************************\n");
								
								double total = 0;
								
								for( Donation donation : donor.getDonations() ) {
									System.out.printf("| %15s | %15s | %10.2f |\n" , 
											donation.getDate(),
											donation.getType(),
											donation.getValue());
									
									total += donation.getValue();
								}
								System.out.printf("|                                ----------------|\n");
								System.out.printf("| %15s   %15s | %10.2f |\n" , "", "Total", total);  
								System.out.printf("**************************************************\n\n");
								
								break;
								
							case 'x':
								continueDonorUtility = false;
							}
						} catch (InputMismatchException e) {
							System.err.printf("\n\nInvalid Selection!\n\n");
							userInput.nextLine();
						}
					} while ( continueDonorUtility );
					
					break;
				case 'n':
					
					System.out.printf("                Donation Utilties                 \n");
					System.out.printf("**************************************************\n\n");
					
					boolean continueDonationUtility = true;
					
					do {
						
						System.out.printf("Please choose an option below:\n");
						System.out.printf("a) Add Donation\n");
						System.out.printf("l) List Donations\n");
						System.out.printf("x) Exit Donations Utilities\n");
						
						
						try {
							
							switch( userInput.next("(?i)[alx]").toLowerCase().charAt(0) ) {
							case 'a':
								userInput.nextLine();
								System.out.printf("                    Add Donation                  \n");
								System.out.printf("**************************************************\n\n");
								Donation d = new Donation();
								
								int count = 0;
								
								for( Donor donor : donors) {
									System.out.printf("%d) %s\n", count++, donor.getName());
								}
								
								System.out.printf("Donor's ID: ");
								Donor donor = donors.get(Integer.parseInt(userInput.nextLine()));
								d.setDonor(donor);
								donor.addDonation(d);
								
								System.out.printf("Donation Date: ");
								d.setDate(userInput.nextLine());
								
								System.out.printf("Donations Type: ");
								d.setType(userInput.nextLine());
								
								System.out.printf("Donations Value(0.00): ");
								d.setValue(Double.parseDouble(userInput.nextLine()));
								
								donors.persist();
								donations.add(d);
								
								System.out.printf("Donation successfully added!\n%s\n", d);
								
								break;
							case 'l':
								userInput.nextLine();
								System.out.printf("                  List Donations                  \n");
								System.out.printf("**************************************************\n\n");
								
								int donationId = 0;
								
								for( Donation donation : donations) {
									System.out.printf("Donation #%d\n", donationId++);
									System.out.printf("**************************************************\n");
									System.out.printf("%s\n\n", donation);
								}
								break;
								
							case 'x':
								continueDonationUtility = false;
							}
						} catch (InputMismatchException e) {
							System.err.printf("\n\nInvalid Selection!\n\n");
							userInput.nextLine();
						}
					} while ( continueDonationUtility );
					
					break;
				case 'x':
					continueDonationsTracker = false;
					break;
				
				}
			} catch (InputMismatchException e) {
				System.err.printf("\n\nInvalid Selection!\n\n");
				userInput.nextLine();
			}
		} while ( continueDonationsTracker );
		
		System.out.printf("      Thanks for using the donation tracker       \n");
		System.out.printf("**************************************************\n");
		System.out.printf("**************************************************\n\n");
		
	}
}
