package com.benjica.csci136.lb9;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * A driver application to demonstrate the XML parser for lab 9.
 * May 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class Lab9 {
	
	public static void main(String[] args) {		
		
		try {
			XMLParser.ValidateXMLString(new File(args[0]));

			System.out.println("The given document validates correctly.");
		} catch (FileNotFoundException e) {
			System.err.println("There was an error in openingn the given file.");
			e.printStackTrace();
		} catch (XMLParseException e) {
			System.err.println("There was an error in parsing the xml document.");
			e.printStackTrace();
		}
		
	}
	
}
