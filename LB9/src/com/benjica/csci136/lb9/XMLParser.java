package com.benjica.csci136.lb9;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * An xml validation function.
 * May 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class XMLParser {

	public static void ValidateXMLString(File document) throws XMLParseException, FileNotFoundException {
		
		// Utility classes
		Stack<String> domStack = new Stack<String>();
		Scanner lineScanner = new Scanner(document);
		
		// A pattern to match tags but exclude the doctype
		Pattern p = Pattern.compile("<[^!][a-zA-Z0-9]*");
		
		do {
			
			String tag = lineScanner.findInLine(p);
			
			while ( tag != null ) {
				
				// Determine if this is a closing tag or not
				if ( tag.charAt(1) == '/' ) {
					
					// pop the current level and compare to make sure proper close tag
					if ( !domStack.pop().equals(tag.substring(2))) 
						throw new XMLParseException(String.format("Found a bad <%s> tag.", tag.substring(1)));
						
				} else {
					// push an open tag
					domStack.push(tag.substring(1));
				}
				
				// See if there is more tags on the line
				tag = lineScanner.findInLine(p);
			}
			
			// Grab the next line
			lineScanner.nextLine();
		} while ( lineScanner.hasNextLine() );
		
		lineScanner.close();
		
		// If you have left over tags you get an excpetion 
		if ( !domStack.empty() ) 
			throw new XMLParseException("The document ended without closing tags.");
	}
}