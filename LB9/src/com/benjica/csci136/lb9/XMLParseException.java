package com.benjica.csci136.lb9;

/**
 * Dummy exception for parse errors.
 * May 7, 2014
 *
 * @author Benjamin Campbell <benjamin.campbell@mso.umt.edu>
 */
public class XMLParseException extends Exception {

	public XMLParseException(String format) {
		super(format);
	}
	
}
