package com.benjica.csci136.tp;

public class TrivialPursuitPlayer {
	
	private String mName;
	private TrivialPursuitToken mToken;
	
	private int mNumberOfGames 	= 0;
	private int mWins			= 0;
	
	/**
	 * Creates a new player with given name
	 * @param name
	 */
	public TrivialPursuitPlayer(String name) {
		mName = name;
		mToken = new TrivialPursuitToken();
	}
	
	/**
	 * Returns the token for the given player.
	 * @return
	 */
	public TrivialPursuitToken getToken() {
		// TODO Auto-generated method stub
		return mToken;
	}
	
	/**
	 * Returns the total number of games played
	 * @return
	 */
	public int getNumberOfGames() {
		return mNumberOfGames;
	}
	
	/**
	 * Returns the total number of wins the players has.
	 * @return
	 */
	public int getWins() {
		return mWins;
	}
	
	/**
	 * Returns the total losses/draws the player has.
	 * @return
	 */
	public int getLosses() {
		return mNumberOfGames - mWins;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return mName;
	}
}
