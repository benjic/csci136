package com.benjica.csci136.tp;

import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * A driver for the console version of the trival pursuit.
 * @author benjica
 *
 */
public class TrivialPursuitDriver {
	
	public static void main(String[] args) throws FileNotFoundException {
		
		// Utility Classes
		Scanner playerInput = new Scanner(System.in);
		

		TrivialPursuitGame game = new TrivialPursuitGame();
		
		D6 dice = new D6();
		
		System.out.printf("**************************************************\n");
		System.out.printf("          Welcome to Trivial Pursuit!             \n");
		System.out.printf("**************************************************\n\n");
		
		boolean doneAddingPlayers;
		
		do {
			
			System.out.printf("Please enter the name of a player.\n");
		
			try {
				
				TrivialPursuitPlayer p = new TrivialPursuitPlayer(playerInput.nextLine());
				
				game.addPlayer(p);
				game.addToken(p.getToken());
				
				System.out.printf("Would you like to add another player?[yN]\n");
				
				if ( playerInput.next("[yY]").toLowerCase().contains("y") ) {
					doneAddingPlayers = false;
				} else {
					doneAddingPlayers = true;
				}
				
				playerInput.nextLine();
				
			} catch ( InputMismatchException e) {
			
				doneAddingPlayers = true;
			}
			
		} while ( !doneAddingPlayers );
		
		System.out.printf("We are now going to roll the dice to see who goes first.\n\n");
		
		TrivialPursuitPlayer highestRoller = new TrivialPursuitPlayer("");
		int highestRoll = 0;
		
		for( TrivialPursuitPlayer p : game.getPlayers()) {
			int roll = dice.roll();
			
			System.out.printf("%s has rolled a %d.\n", p.getName(), roll);
			
			if ( roll > highestRoll ) {
				highestRoll = roll;
				highestRoller = p;
			}
		}
		
		System.out.printf("\n%s has the highest roll and will go first. Press any key to continue.\n\n", highestRoller.getName());
		
		playerInput.nextLine();
		playerInput.nextLine();
		
		for ( int i = game.getPlayers().indexOf(highestRoller); ;i++) {
			TrivialPursuitPlayer currentPlayer = game.getPlayers().get(i % game.getPlayers().size());
			
			int roll = dice.roll();
			
			currentPlayer.getToken().moveToken(roll);
			
			System.out.printf("\n\n\n**************************************************\n");
			System.out.printf("*   %22s                       *\n", currentPlayer.getName());
			System.out.printf("**************************************************\n");
			System.out.printf("%s\n", currentPlayer.getToken().buildTable());
			System.out.printf("You have rolled a %d and landed on %s.\n", roll, currentPlayer.getToken().getCurrentCategory());
			
			TrivialPursuitQuestion q = game.getQuestion(currentPlayer.getToken().getCurrentCategory());
			System.out.printf("\n%s\n", q.getQuestion() );
			
			playerInput.nextLine();
			
			System.out.printf("The correct answer is: %s\n", q.getAnswer());
			
			try {
				System.out.printf("Be honest, %s did you answer the question correctly?[yN]\n\n", currentPlayer.getName());
				
				if ( playerInput.next("[yY]").toLowerCase().contains("y") ) {
					currentPlayer.getToken().addPiece(currentPlayer.getToken().getCurrentCategory());
					
					if ( currentPlayer.getToken().hasCompletedGame()) {
						System.out.printf("!!! CONGRATULATIONS !!!\n");
						System.out.printf("YOU HAVE WON THE GAME OF TRIVIAL PURSUIT\n");
						System.out.println("TRY NOT TO LET IT GO TO YOUR HEAD!\n");
						break;
					} else {
					
						System.out.printf("Wow, go again for answering correctly, %s.\n", currentPlayer.getName());
						i--;
					}
				}
				
				playerInput.nextLine();
				
			} catch ( InputMismatchException e) {
			
				//NOOP
			}
			
			
			
		}
	}
}
