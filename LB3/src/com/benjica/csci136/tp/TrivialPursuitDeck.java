package com.benjica.csci136.tp;

import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

/**
 * A class to abstract a deck of cards in the game. It loads them in from a file and builds a collection of 
 * sub decks to draw questions from.
 * @author benjica
 *
 */
public class TrivialPursuitDeck {
	
	public Map<String, List<TrivialPursuitQuestion>> mDeck = new HashMap<String, List<TrivialPursuitQuestion>>();
	
	/** 
	 * Constructor for building deck from file. This could be repaced with other constrcutors for other sources.
	 * 
	 * @param path
	 * @throws FileNotFoundException
	 */
	public TrivialPursuitDeck(String path) throws FileNotFoundException {
		File triviaFile = new File("./data/trivia.data");
		
		Scanner fileScanner = new Scanner(triviaFile);
		
		while ( fileScanner.hasNextLine()) {
			
			String[] params = fileScanner.nextLine().split("\\*", 2);
			
			
			if ( !mDeck.containsKey(params[0] ) )
				mDeck.put(params[0], new ArrayList<TrivialPursuitQuestion>());
			
			mDeck.get(params[0]).add(new TrivialPursuitQuestion(params[1]));
			
			
		}
		
		fileScanner.close();
	}
	
	/**
	 * Get a question based on category.
	 * 
	 * @param category
	 * @return
	 */
	public TrivialPursuitQuestion drawCard(String category) {
		
		List<TrivialPursuitQuestion> categoryDeck = mDeck.get(category);
		Random rand = new Random();
		
		
		return categoryDeck.get(rand.nextInt(categoryDeck.size()));
		
	}

}
