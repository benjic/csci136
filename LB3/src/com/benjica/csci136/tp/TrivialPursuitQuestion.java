package com.benjica.csci136.tp;

/**
 * A simple data structure for storing questions and parsing
 * incoming questions from the file.
 * @author benjica
 *
 */
public class TrivialPursuitQuestion {

	String mQuestion;
	String mAnswer;
	
	public TrivialPursuitQuestion(String question) {
		
		String[] params = question.split("\\*", 2);
		mQuestion = params[0];
		mAnswer = params[1];
	}

	public String getQuestion() {
		return mQuestion;
	}

	public String getAnswer() {
		return mAnswer;
	}
}
