package com.benjica.csci136.tp.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class TrivialPursuitBoard extends JPanel {

	public TrivialPursuitBoard() {
		this.setBackground( new Color(100,100,150));
		this.setPreferredSize(new Dimension(600,600));
	}

	@Override
	public void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);		
		
		for (int i = 0; i < 46; i++) {
			
			switch( i % 6 ) {
			case 0:
				g.setColor(Color.GREEN);
				break;
			case 1:
				g.setColor(Color.ORANGE);
				break;
			case 2:
				g.setColor(Color.CYAN);
				break;
			case 3:
				g.setColor(Color.YELLOW);
				break;
			case 4:
				g.setColor(new Color(139, 69, 19));
				break;
			case 5:
				g.setColor(Color.PINK);
				break;
				
			}
			
			switch ( i % 7 ){
			case 2:
			case 5:
				// This is a roll again
				g.setColor(Color.GRAY);
			}
			
			
			g.fillArc(50, 50, 
					this.getHeight() - 100,
					this.getHeight() - 100,
					8 * i, 8);
			
			
		}
		
		g.setColor(new Color(100,100,150));
		
		g.fillArc(100, 100, 
				this.getHeight() - 200,
				this.getHeight() - 200,
				0, 360);
		
	}
	

}
