package com.benjica.csci136.tp;

import java.util.HashMap;
import java.util.Map;

/**
 * This keeps track of the token as a device for storing place
 * as well as progress.
 * @author benjica
 *
 */
public class TrivialPursuitToken {

	public final String categories[] = {
		"Geography", 
		"Entertainment", 
		"History",
		"Computers & Video Games", 
		"Science & Nature",
		"Sports & Lesuire" };
	
	private Map<String, Boolean> mCategories =  new HashMap<String, Boolean>();
	private int mCurrentCategory = 0;

	/**
	 * Constructor
	 */
	public TrivialPursuitToken() {
		
		for ( String c : categories ) {
			mCategories.put(c, false);
		}
	}
	
	/**
	 * A test for a player who has a particular category answered.
	 * @param category
	 * @return
	 */
	public boolean hasCategory(String category) {
		return mCategories.containsKey(category);
	}
	
	
	/**
	 * A test to see if all categories have been answered.
	 * @return
	 */
	public boolean hasCompletedGame() {
		for (Boolean p : mCategories.values()) {
			if ( !p ) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * This adds a piece if a question was answered.
	 * @param category
	 */
	public void addPiece(String category) {
		mCategories.put(category, true);
	}
	
	/**
	 * Gets current place on board.
	 * @return
	 */
	public String getCurrentCategory() {
		return categories[mCurrentCategory];
	}
	
	/**
	 * This takes a roll and moves player to new spot.
	 * @param roll
	 */
	public void moveToken(int roll) {
		mCurrentCategory = (mCurrentCategory + roll) % 6;
	}
	
	/**
	 * This builds a visual represenatation of the token.
	 * @return
	 */
	public String buildTable() {
		String table = "**************************************************\n";
		for( Map.Entry<String, Boolean> p : mCategories.entrySet()) {
			table += String.format("* %s * %-42s *\n", 
					p.getValue() ? "*" : " ", 
					p.getKey());
		}
		
		table +=  "**************************************************\n";
		
		return table;
	}
}
