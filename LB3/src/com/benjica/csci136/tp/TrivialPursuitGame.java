package com.benjica.csci136.tp;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;

/**
 * A class to represent the game of trivial pursuit. Duh.
 * 
 * @author benjica
 *
 */
public class TrivialPursuitGame {
	
	// Attributes for a typical game of Trivial Pursuit
	private List<TrivialPursuitPlayer> mPlayers;
	private List<TrivialPursuitToken> mTokens;
	private TrivialPursuitDeck mDeck;
	
	/**
	 * Class constructor.
	 * @throws FileNotFoundException
	 */
	public TrivialPursuitGame() throws FileNotFoundException {
		mPlayers = new ArrayList<TrivialPursuitPlayer>();
		mTokens = new ArrayList<TrivialPursuitToken>();
		
		mDeck = new TrivialPursuitDeck("./data/trivia.data");
	}
	
	/**
	 *  Add a player to the game.
	 *  
	 * @param player
	 */
	public void addPlayer(TrivialPursuitPlayer player) {
		
		mTokens.add(player.getToken());
		
		mPlayers.add(player);
	}

	/**
	 * Register a token for each player
	 * @param token
	 */
	public void addToken(TrivialPursuitToken token) {
		mTokens.add(token);
		
	}

	/**
	 * Get all players.
	 * @return
	 */
	public List<TrivialPursuitPlayer> getPlayers() {
		// TODO Auto-generated method stub
		return mPlayers;
	}
	
	/**
	 * Get a question from the deck.
	 * @param category
	 * @return
	 */
	public TrivialPursuitQuestion getQuestion(String category) {
		
		return mDeck.drawCard(category);
	}

}
