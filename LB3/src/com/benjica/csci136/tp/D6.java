package com.benjica.csci136.tp;

import java.util.Random;

/**
 * A class for rolling dice. A classy way of emulating chance.
 * @author benjica
 *
 */
public class D6 {

	private Random mRand = new Random();
	
	/**
	 * Generate a semi random number between 1 and 6
	 * @return
	 */
	public int roll() {
		return mRand.nextInt(5) + 1;
	}
}
